<?php

use Illuminate\Database\Seeder;

class PropertyTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['name' => 'House'],
            ['name' => 'Flat'],
            ['name' => 'Bungalow'],
            ['name' => 'Apartment / Studio'],
            ['name' => 'Farm or Small Holding'],
            ['name' => 'Chalet'],
            ['name' => 'Commercial'],
            ['name' => 'Building Plot'],
        ];

        DB::table('property_types')->insert($types);
    }
}
