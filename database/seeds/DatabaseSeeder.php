<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Branches::class);
        $this->call(PropertyStyles::class);
        $this->call(PropertyTypes::class);
    }
}
