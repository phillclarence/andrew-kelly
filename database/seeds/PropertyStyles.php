<?php

use Illuminate\Database\Seeder;

class PropertyStyles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $styles = [
            ['name' => 'Detached'],
            ['name' => 'Semi Detached'],
            ['name' => 'Terraced'],
            ['name' => 'End Terrace'],
            ['name' => 'Ground Floor Flat'],
            ['name' => 'Upper Floor Flat'],
            ['name' => 'Commercial'],
        ];

        DB::table('property_styles')->insert($styles);
    }
}
