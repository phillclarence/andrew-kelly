<?php

use Illuminate\Database\Seeder;

class Branches extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branches = [
            ['name' => 'Andrew Kelly Rochdale'],
            ['name' => 'Andrew Kelly Heywood'],
            ['name' => 'Andrew Kelly Littleborough'],
            ['name' => 'Andrew Kelly Milnrow'],
            ['name' => 'Andrew Kelly Rawtenstall'],
            ['name' => 'Andrew Kelly Auctions'],
            ['name' => 'Andrew Kelly Lettings'],
            ['name' => 'Andrew Kelly &amp; Associates'],
        ];

        DB::table('branches')->insert($branches);
    }
}
