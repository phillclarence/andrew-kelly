<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('branch_id')->index();
            $table->integer('property_reference')->index();
            $table->integer('property_type_id')->index();
            $table->integer('property_style_id')->index();

            $table->string('price_text')->nullable();
            $table->decimal('numeric_price', 10, 2)->nullable();
            $table->integer('bedrooms')->nullable();
            $table->integer('receptions')->nullable();
            $table->integer('bathrooms')->nullable();
            $table->string('priority', 50)->nullable();
            $table->string('tenure', 50)->nullable();

            $table->text('advert_heading')->nullable();
            $table->text('main_advert')->nullable();
            $table->text('advert2')->nullable();
            $table->text('advert3')->nullable();
            $table->text('advert4')->nullable();
            $table->text('advert5')->nullable();
            $table->text('advert6')->nullable();

            $table->string('house_number')->nullable();
            $table->string('street')->nullable();
            $table->string('district')->nullable();
            $table->string('town')->nullable();
            $table->string('county')->nullable();
            $table->string('postcode', 10)->nullable();
            $table->string('area')->nullable();

            $table->decimal('latitude', 9, 6)->nullable()->index();
            $table->decimal('longitude', 9, 6)->nullable()->index();

            $table->boolean('new_home')->default(0);
            $table->boolean('no_chain')->default(0);
            $table->string('furnished', 20)->nullable();
            $table->boolean('featured_property')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
