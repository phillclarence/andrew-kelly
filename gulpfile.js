var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'public/css/style.css');
});

elixir(function(mix) {
	var scripts = [
		'../../../node_modules/jquery/dist/jquery.min.js',
		'vendor/jquery.flexslider-min.js',
		'vendor/jquery.slideme2.js',
		'../../../node_modules/owlcarousel/owl-carousel/owl.carousel.js',
		'frontend.js',
		'PropertySearch.js'
	];

	mix.scripts(scripts, 'public/js/frontend.min.js');
});

elixir(function(mix) {
   mix.browserSync({
	   proxy:'andrewkelly.dev',
	   open: false,
	   notify: false,
   });
});