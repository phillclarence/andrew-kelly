var propertyType = $('#property-type');
var propertyTypeOption = $('#property-search .property-type a');
var rentRange = $('select[name=minRent], select[name=maxRent]');
var priceRange = $('select[name=minPrice], select[name=maxPrice]');
var area = $('#area');

propertyTypeOption.on('click', function(e) {
    e.preventDefault();

    var type = $(this).attr('data-type');
    setPropertyType(type);
    setPriceRange(type);
});

var setPropertyType = function(type) {
    propertyType.val(type);

    propertyTypeOption.removeClass('active');
    $('#property-search .property-type a[data-type='+type+']').addClass('active');
};

var setPriceRange = function(type) {
    if (type == 'lettings') {
        priceRange.hide();
        rentRange.show();
    } else {
        rentRange.hide();
        priceRange.show();
    }
};

var initSearch = function() {
    var type = $('body').data('type');

    if (!type) {
        type = $('#previous-type').val();
    }

    if (!type) {
        type = 'sales';
    }

    // Set Property Type based on current page type
    setPropertyType(type);
    setPriceRange(type);
};

initSearch();


/* Google API Search Location
 ======================================== */
var init_location = function() {
    var input = document.getElementById('area');

    var options = {
        componentRestrictions:{country:'gb'}
    };

    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function(){
        if (input.value != "") {
            var place = autocomplete.getPlace();
            console.log(place);

            document.getElementById('lat').value = place.geometry.location.lat();
            document.getElementById('lng').value = place.geometry.location.lng();

            // Extract a location value
            if(place.vicinity) {
                input.value = place.vicinity;
            } else {

                $.each(place.address_components, function(key, value) {
                    if(!$.inArray("postal_town", value.types)) {
                        input.value = value.long_name;
                    }
                });
            }
        }
    });
};

init_location();

area.on('keyup', function(e) {
    console.log('changed');
    if ($(this).val() == '') {
        document.getElementById('lat').value = '';
        document.getElementById('lng').value = '';
    }
});