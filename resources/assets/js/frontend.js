$(window).load(function(){
	$('body').addClass('loaded');
});

$(document).ready(function($){

	/* Masthead Slider
	 ============================================== */
		var mastheadSlider = $('.masthead');

		mastheadSlider.slideme({
			arrows: false,
			autoslide: true,
			pagination: false,
			transition: 'fade',
			loop: true,
			interval: 2500,
		});

	/* Mobile Navigation
	 ============================================== */
		var mobileNavTrigger = $('.mobile-nav-trigger');
		var navigation = $('.mobile-site-navigation');

		mobileNavTrigger.on('click', function(e) {
			navigation.slideToggle();
		});

	/* Card Carousel
	============================================== */
		var cardCarouselProperties = $('.card-carousel.properties');
		var cardCarouselAgents = $('.card-carousel.agents');
		var cardCarouselPrev = $('.card-carousel-nav .prev');
		var cardCarouselNext = $('.card-carousel-nav .next');

		cardCarouselProperties.owlCarousel({
			singleItem: false,
			pagination: false,
			items: 3,
			itemsDesktop: false,
		});

		cardCarouselAgents.owlCarousel({
			singleItem: false,
			pagination: false,
			items: 3,
			itemsDesktop: false,
		});

		cardCarouselNext.on('click', function(){
			var carousel = $(this).attr('data-carousel');

			if(carousel == 'agents') {
				cardCarouselAgents.trigger('owl.next');
			}

			if(carousel == 'properties') {
				cardCarouselProperties.trigger('owl.next');
			}
		});

		cardCarouselPrev.on('click', function(){
			var carousel = $(this).attr('data-carousel');

			if(carousel == 'agents') {
				cardCarouselAgents.trigger('owl.prev');
			}

			if(carousel == 'properties') {
				cardCarouselProperties.trigger('owl.prev');
			}
		});

	/* Testimonials Carousel
	 ============================================== */

		var testimonialCarousel = $('.testimonials-carousel');

		if(testimonialCarousel.length) {
			testimonialCarousel.owlCarousel({
				singleItem: true,
				pagination: false,
				autoPlay: true,
				transitionStyle: 'fade',
				autoHeight: true,
			})
		}

	/* Property Listing Gallery
	 ============================================== */
		var propertyListingCarousel = $('.PropertyListing__carousel');
		var propertyListingSlider = $('.PropertyListing__slider');

		propertyListingCarousel.flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 190,
			itemMargin: 0,
			asNavFor: '#slider'
		});

		propertyListingSlider.flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			smoothHeight: true,
			sync: "#carousel"
		});

	/* Modal Loader
	 ============================================== */
		var modalTrigger = $('a.modal-trigger');

		modalTrigger.on('click', function(e){
			e.preventDefault();

			var modal = $(this).attr('data-modal');
			var property = $(this).attr('data-property');
			var url = "/modal/" + modal;

			if (property) {
				url = url + '/' + property;
			}

			if($('.FormModal').length) {
				$('.FormModal').remove();
			}

			$.ajax({
				type: "GET",
				url: url,
				success: function(response){
					var type = $('body').data('type');
					$('body').append(response).css({ overflow: 'hidden' });
					$('.page').addClass('page--blur');
					$('input[name="type"]').val(type);
				}
			});

		});

		$('body').on('click', '.FormModal__close', function(e) {
			e.preventDefault();
			$('.FormModal').remove();
			$('.page').removeClass('page--blur');
            $('body').css({ overflow: '' });
		});

		$('body').on('click', '.FormModal__overlay', function(e) {
            $('.FormModal').remove();
            $('.page').removeClass('page--blur');
            $('body').css({ overflow: '' });
		});

	/* Modal Form Submit
	 ============================================== */

		$(document).on('submit', '.modal-form', function(e) {
			e.preventDefault();

			var el = $(this);

			var modal = el.attr('data-modal-type');
			var payload = el.serialize();
			var action = el.attr('action');

			$('.alert').remove();

			$.ajax({
				type: "POST",
				data: payload,
				dataType: "JSON",
				url: "/modal/"+modal,
				success: function(response){
					el.append('<ul class="ui-form__message alert alert-success"><li>'+ response.message + '</li></ul>');
					el.trigger('reset');
				},
				error: function(response) {
					var errors = jQuery.parseJSON(response.responseText);
					handleErrors(errors, el);
				}
			});

			return false;
		});

		var handleErrors = function(errors, form) {

			form.append('<ul class="ui-form__message alert alert-danger"></ul>');

			$.map(errors, function(key) {
				$('.alert').append('<li>'+ key[0] +'</li>');
			});

		}

	/* Property Search Toggle
	 ============================================== */

		var searchProperty = $('.search.property');
		var searchToggleTrigger = $('.search-toggle-trigger');

		if(searchProperty.length) {
			searchProperty.hide();
		}

		searchToggleTrigger.on('click', function(e) {
			searchToggleTrigger.toggleClass('active');
			searchProperty.slideToggle();
		});

	/* Property Sort By
	 ============================================== */
	
		var sortProperties = $('#sort-properties');
		var sortPropertiesForm = $('#sort-properties-form');

		sortProperties.on('change', function(e) {
			var value = $(this).val();
			console.log(value);

			$("#property-search input[name='sort']").val(value);
			$('form#property-search').submit();

		});

	/* Smooth Scroll to #anchor
	 ============================================== */

		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
});