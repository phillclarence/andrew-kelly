@extends('emails.master')
@section('preheader', 'Mailing List Submission')

@section('content')
    <h1>Mailing List Submission</h1>

    <p>&nbsp;</p>

    <h3>Personal Details</h3>

    <p><strong>Name:</strong> {!! $input->name !!}</p>
    <p><strong>Address:</strong>
        {!! $input->address_1 !!}
        {!! $input->address_2 ?: null !!}
        {!! $input->postcode !!}
    </p>
    <p><strong>Phone:</strong> {!! $input->phone !!}</p>
    <p><strong>Mobile:</strong> {!! $input->mobile !!}</p>
    <p><strong>Email:</strong> {!! $input->email !!}</p>

    <p>&nbsp;</p>

    <h3>Property Criteria</h3>

    <p><strong>Price range:</strong> {!! $input->min_price !!} - {!! $input->max_price !!}</p>
    <p><strong>Preferred Areas:</strong></p>
    <p>{!! $input->areas !!}</p>
@endsection