@extends('emails.master')
@section('preheader', 'Viewing Request')

@section('content')
    <h1>Viewing Request</h1>

    <p><strong>Viewing type:</strong> {!! $input->type !!}</p>
    <p><strong>Name:</strong> {!! $input->name !!}</p>
    <p><strong>Email:</strong> {!! $input->email !!}</p>
    <p><strong>Phone:</strong> {!! $input->phone !!}</p>
    <p><strong>Mobile:</strong> {!! $input->mobile !!}</p>

    <hr>

    <p><strong>Address:</strong>
        {!! $input->address !!}
        {!! $input->address1 ? ', ' . $input->address1 : null !!}
    </p>
    <p><strong>Postcode:</strong> {!! $input->postcode !!}</p>

    <hr>

    <p><strong>Date:</strong> {!! $input->date !!}</p>
    <p><strong>Time:</strong> {!! $input->time !!}</p>
@endsection