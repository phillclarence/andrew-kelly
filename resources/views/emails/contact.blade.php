@extends('emails.master')
@section('preheader', 'Customer Enquiry')

@section('content')
    <h1>Customer Enquiry</h1>

    <p>&nbsp;</p>

    <p><strong>Name:</strong> {!! $input->name !!}</p>
    <p><strong>Email:</strong> {!! $input->email !!}</p>

    <p><strong>Message:</strong></p>
    <p>{!! $input->message !!}</p>
@endsection