@extends('emails.master')
@section('preheader', 'Valuation Request')

@section('content')
    <h1>Valuation Request</h1>

    <p><strong>Valuation type:</strong> {!! $input->type !!}</p>
    <p><strong>Name:</strong> {!! $input->name !!}</p>
    <p><strong>Email:</strong> {!! $input->email !!}</p>
    <p><strong>Phone:</strong> {!! $input->phone !!}</p>
    <p><strong>Mobile:</strong> {!! $input->mobile !!}</p>

    <hr>

    <p><strong>Address:</strong>
        {!! $input->address !!}
        {!! $input->address1 ? ', ' . $input->address1 : null !!}
    </p>
    <p><strong>Postcode:</strong> {!! $input->postcode !!}</p>

    <hr>

    <p><strong>Message:</strong><br/>
        {!! $input->comments !!}
    </p>
@endsection