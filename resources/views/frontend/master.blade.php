<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')Andrew Kelly</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="{!! asset('css/style.css?v=1') !!}">
</head>
<body id="app" data-type="@if (isset($type)){{ $type }}@endif">
	<header class="site-header">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<a href="{!! route('home') !!}">
							<img src="{!! asset('images/brand/andrew-kelly.png') !!}" alt="Andrew Kelly" width="220" height="37">
						</a>

						<a href="#" class="visible-xs mobile-nav-trigger">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					<div class="col-md-9">
						<nav class="site-navigation hidden-xs">
							<ul>
								<li><a href="{!! route('home') !!}">Home</a></li>
								<li><a href="{!! route('properties') !!}">Properties</a></li>
								<li><a href="{!! route('sales') !!}">Sales</a></li>
								<li><a href="{!! route('lettings') !!}">Lettings</a></li>
								<li><a href="{!! route('auctions') !!}">Auctions</a></li>
								<li><a href="{!! route('services') !!}">Services</a></li>
								<li><a href="{!! route('blog') !!}">Blog</a></li>
								<li><a href="{!! route('contact') !!}">Contact</a></li>
							</ul>
						</nav>
						<nav class="mobile-site-navigation hidden-sm hidden-md hidden-lg">
							<ul>
								<li><a href="{!! route('home') !!}">Home</a></li>
								<li><a href="{!! route('properties') !!}">Properties</a></li>
								<li><a href="{!! route('sales') !!}">Sales</a></li>
								<li><a href="{!! route('lettings') !!}">Lettings</a></li>
								<li><a href="{!! route('auctions') !!}">Auctions</a></li>
								<li><a href="{!! route('services') !!}">Services</a></li>
								<li><a href="{!! route('blog') !!}">Blog</a></li>
								<li><a href="{!! route('contact') !!}">Contact</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>

	<div class="page">
		<main class="site-content">
			@yield('content')
		</main>

		<footer class="site-footer">
			<section class="footer-upper">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<h6 class="heading">Andrew Kelly</h6>
							<p>If you are looking to buy, sell, rent or let a house / home, arrange mortgage finance or insurance, source an EPC, deal with conveyancing or legal matters, obtain investment advice or buy / sell through auction in the Rochdale districts or Rossendale Valley, we are the market leaders.</p>
						</div>
						<div class="col-md-4">
							<h6 class="heading">Contact Details</h6>
							<address>
								124 Yorkshire Street,<br/> Rochdale,<br/> Lancashire<br/> OL16 1LA
							</address>
							<p>
								<strong>Sales</strong><br/>
								Tel: 01706 350363<br/>
								Email: <a href="mailto:rochdale@andrew-kelly.co.uk">rochdale@andrew-kelly.co.uk</a>
							</p>
							<p>
								<strong>Lettings</strong><br/>
								Tel: 01706 352266<br/>
								Email: <a href="mailto:lettings@andrew-kelly.co.uk">lettings@andrew-kelly.co.uk</a>
							</p>
						</div>

						<div class="col-md-4">
							<h6 class="heading">Navigation</h6>
							<nav class="footer-nav">
								<ul>
									<li><a href="{!! route('home') !!}">Home</a></li>
									<li><a href="{!! route('sales') !!}">Residential Sales</a></li>
									<li><a href="{!! route('lettings') !!}">Residential Lettings</a></li>
									<li><a href="{!! route('auctions') !!}">Property Auctions </a></li>
									<li><a href="{!! route('services.show', 'conveyancing') !!}">Conveyancing &amp; Legal</a></li>
									<li><a href="{!! route('services.show', 'epc') !!}">EPCs</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-lower">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p>ANDREW KELLY &amp; ASSOCIATES &copy; {!! date('Y') !!} ALL RIGHTS RESERVED. <a href="
							http://www.hypaconcept.com" target="_blank">WEB DESIGN HYPA CONCEPT</a></p>
						</div>
					</div>
				</div>
			</section>
		</footer>
	</div>

	@yield('scripts')
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAfr0so2l6Qa76gwANLFtvACbEaiexOxm8&libraries=places"></script>
	<script type="text/javascript" src="{!! asset('js/frontend.min.js?v=1') !!}"></script>
</body>
</html>