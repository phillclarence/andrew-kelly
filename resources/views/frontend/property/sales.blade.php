@extends('frontend.master')

@section('title', 'Residential Sales - ')

@section('content')

	@include('frontend.partials.masthead')
	
	<!-- Sales -->
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h1 class="section-heading">Residential Sales</h1>
					<hr>
				</header>

				<article class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<p>Find homes for sale in the Rochdale Districts, Heywood, Rossendale & Todmorden with the largest independent residential estate agent in the area…. Andrew Kelly & Associates.</p>
							<p>Using our local knowledge, experienced staff and comprehensive range of services, we have helped thousands of people move house. Few estate agents in the UK (let alone the North West) can offer such a total solution to moving house and our service range has now grown to include general estate agency, lettings & property management, mortgages and insurance, investments, Energy Performance Certificates (EPCs), legal help through our conveyancing partner TTPL and property sales by Auction. Whatever your needs, it is almost certain that we can not only help, but also guide you through this important and complicated process. Our policy is to provide you with a high level of service through personal teams, based at local offices, but advertised with an array of marketing solutions that only a company of this size can sustain. We have a dedicated </p>
						</div>
						<div class="col-md-6">
							<p>customer care model which ensures that you will have regular contact from the staff charged with marketing your home and helping you through the sale process. The Valuer who originally advised you on the sale of your home will stay personally involved with the sale strategy through the marketing period. All of Andrew Kelly’s directors work directly in the field, meeting customers and staying close to market conditions and customers' needs.</p>
							<p>Important reasons to choose Andrew Kelly & Associates is our local estate agency knowledge, this is vital as most people move home within a 7 mile radius of where they live (due to work / family / schools). We have multiple offices spread across a wide but local geographic area. Your property is advertised regularly in the local press. We currently take large sections in the Rochdale Observer Homesearch and Heywood Advertiser spreading the net for your property as wide as possible (within the local geographic boundaries) to ensure that potential purchasers see your home. No other agent in the area offers the same coverage.</p>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>

	@include('frontend.partials.callouts', ['class' => 'grey'])
	
	<!-- Buying Advice -->
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h2 class="section-heading">Buying Advice</h2>
					<hr>
				</header>

				<article class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<p>There are very few purchasers who buy the home of their dreams and do not want to change anything. All too often when you view a property, you see it as it is - not as it could be. The best way to view a property is to have an open mind because a property can be altered to your own tastes and requirements.</p>
							<p>We recommend that you view as many properties within your price range and preferred location to begin to understand what is on offer in terms of style, size and condition. Our staff will be happy to search through a range of properties with you and can tailor the search according to many different criteria (according to your needs). If you register for our mailing list, our experienced negotiators will be able to send properties to you via sms text, email or post and you can be first to hear about new properties added to our register.</p>
						</div>
						<div class="col-md-6">
							<p>Always bear in mind that decoration, fixtures & fittings and sometimes even layouts can be changed to suit your lifestyle. Try to purchase with your head and not your heart. Please call into your local branch (or call us) if you would like to pick up (or be sent) a copy of our “guide to moving house” which provides a wealth of valuable home moving information.</p>
							<p>Please also remember that the most important part of the purchase process is to have your finances in place and for you to understand what you can afford. A seller is far more likely to take offers seriously if they know that you have a mortgage arranged in principle. “click here to go to Financial Services section”</p>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>

	<!-- Selling Advice -->
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h2 class="section-heading">Selling Advice</h2>
					<hr>
				</header>

				<article class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<p>Selling your property quickly and for the best possible price is always a priority and there are a few simple steps that can greatly improve your chances of making a good first impression to potential buyers:</p>
							<p><strong>Exterior</strong></p>
							<p>Many people will “drive by” your home prior to booking a viewing. Try to ensure that the exterior of your property is always well maintained - not just for viewings. The frontage is a good place to spend a little money. A tidy garden gives the impression of a well kept home. Bedding plants & flowers are not generally expensive but will improve the image. A fresh coat of paint can work wonders to the appearance of external fences, doors and window sills. Clearing out garages and external stores can give the impression of more space.</p>
							<p><strong>Interior</strong></p>
							<p>Make sure that you home is clean throughout. A good spring clean always helps and don’t forget the windows. Try to ensure the interior of your home is free of clutter, particularly</p>
						</div>
						<div class="col-md-6">
							<p>in hallways and in smaller rooms. Consider the position of furniture to make the best use of the space. Clutter makes homes feel smaller and less organised. Objects such as photographs and ornaments can personalise the home to you and can make it more difficult for a potential purchaser to identify it as their home.</p>
							<p>Opening the windows in summer can give a brighter, light and airy feel. Ensure there is adequate lighting in every room, particularly in Winter. Try to keep décor neutral and light to increase the feel quality and spaciousness. Not all buyers view the potential of a home….only as it is currently presented. Simple is always safest. Not everyone likes pets so if you have a viewing booked, try to keep them isolated. Use objects to enhance the space such as mirrors which will help to reflect the light and make areas look bigger.</p>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>
	
	@include('frontend.partials.testimonials')

	<!-- Sales Progression -->
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h2 class="section-heading">Sales Progression</h2>
					<hr>
				</header>

				<article class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<p>Estate agency is not just about marketing your property and negotiating offers. Most of the work we undertake, in helping to sell your home, takes place after the offer has been received. There are many stages of the buying / selling process that need to be completed including taking formal identification of the parties involved, arranging surveys, confirming finances, liaising with mortgage lenders, issuing memorandums of sale and assisting in the instruction of any additional reports that may be required.</p>
							<p>Our staff have detailed knowledge of the sale progression process and our role is to liaise between buyers, sellers and solicitors and to ensure that the information needed to complete the sale has been obtained at the correct time. Synchronizing purchase “chains” can become complicated and you can be sure that our staff are here to help and guide you through the journey.</p>
							<p>It is an unfortunate fact that chains break down from time to time. We will try to do everything in our power to prevent this from happening.</p>
						</div>
						<div class="col-md-6">
							<p>You can help the process to run more smoothly in many ways including:<br/>Having your finances agreed in principle before making an offer on a property. Instructing your Solicitor as soon as possible to ensure they are ready to act when required. Ensure you comply with requests for information as soon as possible and complete the fixtures and fittings enquiry and return it your solicitor as soon as possible.</p>
							<p>Ensure that ground rents or management / maintenance charges are up to date plus a very important point that is often overlooked is to try to be civil and impersonal. Property purchases can sometimes involve high levels of uncertainty, with both financial and emotional issues depending on the outcome. Try to see the transaction from the other party’s point of view and keep a level head. Remember, if you have any doubts or problems, our staff are here to try and help.</p>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>

	@include('frontend.partials.accreditations')

@stop