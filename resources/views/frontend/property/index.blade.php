@extends('frontend.master')

@section('content')

    <!-- Property Search -->
    <section class="search property">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('frontend.partials.property-search')
                </div>
            </div>
        </div>
    </section>

    <div class="search-toggle">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="title">Search Filter</span>
                    <span class="search-toggle-trigger"><i class="fa fa-chevron-down"></i></span>
                </div>
            </div>
        </div>
    </div>

    <div class="search-sort">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <form id="sort-properties-form" class="ui-form" action="{!! request()->fullUrl() !!}" method="get">
                        <span class="ui-select ui-select--dark">
                            <select name="sort" id="sort-properties">
                                <option value="recent" @if(request()->get('sort') == 'recent') selected @endif>Most Recent</option>
                                <option value="low-high" @if(request()->get('sort') == 'low-high') selected @endif>Price Low to High</option>
                                <option value="high-low" @if(request()->get('sort') == 'high-low') selected @endif>Price High to Low</option>
                            </select>
                        </span>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <section class="section property PropertyResults">
        <div class="container">
            <div class="row">
                <header class="Pagination__header col-md-12">
                    <span class="Pagination__results">{!! $properties->total() !!} result(s) found</span>
                    <span class="Pagination__summary">Page {!! $properties->currentPage() . ' of ' . $properties->lastPage() !!}</span>
                </header>
            </div>
            <div class="row">
                @if($properties->count())
                    @foreach($properties as $property)
                        <div class="col-md-4 col-sm-6">
                            @include('frontend.partials.property.property-card')
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12 text-center">
                        <h2>No properties found which match your search criteria</h2>
                    </div>
                @endif
            </div>

            <div class="row">
                <div class="Pagination col-md-12 text-center">
                    <div class="Pagination__pages">
                        {!! $properties->appends(request()->except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('frontend.partials.callouts', ['class' => ''])

    @include('frontend.partials.testimonials')

    @include('frontend.partials.accreditations')

@stop