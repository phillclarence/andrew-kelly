@extends('frontend.master')

@section('title', 'Residential Lettings - ')

@section('content')

    @include('frontend.partials.masthead')

    <!-- Residential Lettings -->
    <section class="section article">
        <div class="container">
            <div class="row">
                <header class="section-header col-md-12">
                    <h1 class="section-heading">Residential Lettings</h1>
                    <hr>
                </header>

                <article class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <p>Andrew Kelly Lettings & Property Management is a long established company and is a regulated and accredited letting and management agent. If you are looking for a property to let, we have a large selection to choose from, all of which can be viewed by clicking the link above. The properties currently available to let are only a small proportion of the portfolio we manage and if you can’t find one that suits your needs, register your details and requirements with us and we can let you know as soon as one becomes available.</p>
                        </div>
                        <div class="col-md-6">
                            <p>Andrew Kelly Property Management provides a professional and personal service for both landlords and tenants. Lettings and property management is a specialised field within estate agency and we have an experienced, dedicated and friendly team offering individual service, unmatched by other agents. We are proud to have become Rochdale’s leading lettings and property management agent and to have achieved accreditation from Rochdale Metropolitan Borough Council.</p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    @include('frontend.partials.callouts', ['class' => 'grey'])

    <!-- Tenant Information -->
    <section class="section article">
        <div class="container">
            <div class="row">
                <header class="section-header col-md-12">
                    <h2 class="section-heading">TENANT INFORMATION</h2>
                    <hr>
                </header>

                <article class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <p>If you are a tenant looking for a property to rent, Andrew Kelly Lettings & Property Management can help you find a property to suit your needs and guide you every step of the way through the rental process.</p>
                        </div>
                        <div class="col-md-6">
                            <p>From the moment of contact you will find our team, friendly, helpful and knowledgeable, with the personal touch that can make all the difference.</p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <!-- Tenant Fess Information -->
    <section class="section article">
        <div class="container">
            <div class="row">
                <header class="section-header col-md-12">
                    <h2 class="section-heading">TENANT FEES INFORMATION</h2>
                    <hr>
                </header>

                <article class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <p>There is a £180 Application Fee per property, payable when applying for the tenancy of the property which covers referencing fees including credit checks, bank and previous landlord. There is no extra guarantor fees. There is an administration fee of £100 inclusive of VAT for drawing up the tenancy agreement, which also includes registration of the deposit with the DPS (Deposit Protection Service) and a preparation of an Inventory.</p>
                            <p>Most of our properties are subject to the first month's rent and a bond equivalent to one month's rent which is payable prior to moving in, please ring 01706 352266 to check if this is the case with the property you are interested in. </p>
                        </div>
                        <div class="col-md-6">
                            <p>Should the tenant want to renew their contract after expiration of the initial contract then a fee of £42 inclusive of VAT will be payable and then on each subsequent renewal.</p>
                            <ul>
                                <li>• When making a payment by Debit Card you will be subject to a charge equal to 1% of the payment value.</li>
                                <li>• When making a payment by Credit Card you will be subject to a charge equal to 2% of the payment value.</li>
                                <li>• Andrew Kelly Property Management are a member of the Client Money Protection Scheme</li>
                                <li>• Andrew Kelly Property Management are a member of the TPO Redress Scheme</li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    @include('frontend.partials.testimonials.lettings')

    <!-- Landlord Information -->
    <section class="section article">
        <div class="container">
            <div class="row">
                <header class="section-header col-md-12">
                    <h2 class="section-heading">LANDLORD INFORMATION</h2>
                    <hr>
                </header>

                <article class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <p>Are you thinking of letting a property? Andrew Kelly Property Management has a specialised team in place to advise and action every stage of the letting process. You don’t need to leave your armchair, a qualified staff member will come to your home, if you so wish, undertake a free market appraisal, explain all aspects of the letting process and answer any questions you may have.</p>
                            <p>Our services are designed to put you the landlord first. Andrew Kelly Property Management is a regulated and accredited agent so you can rest assured of the successful management of your property.</p>
                        </div>
                        <div class="col-md-6">
                            <p>Our Comprehensive Management Service Includes:</p>
                            <ul>
                                <li>• Free market appraisal</li>
                                <li>• Free professional letting advice</li>
                                <li>• Letting only service</li>
                                <li>• Full management</li>
                                <li>• Extensive marketing & advertising</li>
                                <li>• Tenant professional referencing & credit check</li>
                                <li>• Computerised system regularly updated on legislation & agreements</li>
                                <li>• 24 hour emergency maintenance team</li>
                                <li>• Inventory service</li>
                                <li>• Portfolio management</li>
                                <li>• Deposit protection</li>
                                <li>• No let no fee</li>
                            </ul>

                            <p>Andrew Kelly Lettings & Property Management are THE Rochdale Letting Specialists.</p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <!-- Landlord Fees Information -->
    <section class="section article">
        <div class="container">
            <div class="row">
                <header class="section-header col-md-12">
                    <h2 class="section-heading">LANDLORD FEES INFORMATION</h2>
                    <hr>
                </header>

                <article class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <p>We are required to explain our charges to Landlords. This can be quite difficult as we
                                provide different services to suit every Landlords needs. We have set out below our
                                general charges.</p>
                            <p>FULL MANAGEMENT 12% of the rent collected inclusive of VAT each month.</p>
                            <p>In addition to the above there is a setup fee/tenancy agreement charge which is not payable in advance but will be collected from the first month’s rent in addition to the above.</p>
                            <p>SETUP CHARGE/TENANCY AGREEMENT FEE - £199 + VAT</p>
                        </div>
                        <div class="col-md-6">
                            <p>LET ONLY SERVICE – An amount equal to the first month’s rent Inclusive of VAT Please ring us to discuss the fees that would apply in your particular circumstance. Andrew Kelly Property Management are a member of the Client Money Protection Scheme</p>
                            <p>Andrew Kelly Property Management are a member of the TPO Redress Scheme</p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    @include('frontend.partials.accreditations')

@stop