@extends('frontend.master')

@section('title', 'Auctions - ')

@section('content')

    @include('frontend.partials.masthead')

    <!-- Sales -->
    <section class="section article">
        <div class="container">
            <div class="row">
                <header class="section-header col-md-12">
                    <h1 class="section-heading">Auctions</h1>
                    <hr>
                </header>

                <article class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <p>At Andrew Kelly &amp; Associates we believe in offering you a complete property service. Andrew Kelly Auctions is a part of that service. We have grown to become the LOCAL market leader in general estate agency in the Rochdale Districts and Rossendale Valley yet generally, within these districts, Property Auctions have only been undertaken by National Auction Houses. With this in mind, we have invested again to produce yet another LOCAL service to our clients - one which is exciting and provides superb opportunities for both buyers and sellers of properties - another service which is a natural synergy to the expertise and local knowledge already contained within the Andrew Kelly Group.</p>
                            <p>People often believe that buying or selling property by Auction is different, complicated or risky and should only be attempted by experienced property investors, developers or builders. Today, Property Auctions can be an effective way</p>
                        </div>
                        <div class="col-md-6">
                            <p>for you to move home, to find an improvement project or to acquire a buy-to-let investment. We hold regular sales to allow you to plan ahead.</p>
                            <p>Auction is straightforward, it is easy, and with us it can be even easier! Like anything that is easy, we make sure that each step that you take is simple and that YOU are in control. Whether Buying or Selling you are part of what is simply - another way - another way of what you want to achieve – to buy or to sell. At Andrew Kelly &amp; Associates, we believe in a seamless process based upon our being an integral part of the process - thought – decision - buying or selling - finance - legals - EPCs and at every stage we are there for you. How? We know the whole house moving process.</p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    @include('frontend.partials.callouts', ['class' => 'grey'])

            <!-- Buying Advice -->
    <section class="section article">
        <div class="container">
            <div class="row">
                <header class="section-header col-md-12">
                    <h2 class="section-heading">A Guide For Sellers</h2>
                    <hr>
                </header>

                <article class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <p>There are many advantages to selling at auction, the main ones - it’s quick - the whole process takes around six weeks, there’s no chain, and when the hammer falls your property is sold. Selling at Auction also widens the potential market, not only will there be buyers who want to buy a property to make it a home, there may also be investors, developers and property dealers.</p>
                            <p>The first thing that you need to do is to find out what your property is worth. (click here for an auction appraisal) Once you’ve decided to sell you then need a full Auction appraisal to find out what price you can expect to achieve, the likely timescales and costs involved. You will need to register your property with us at least five weeks before a scheduled sale date to have it included in the catalogue and allow the Auctioneer time to market it. Andrew Kelly Auctions will market your home in the local or national press, distribute the catalogue to potential buyers, and keep you fully updated with the response.</p>
                            <p>You’ll also need to appoint a solicitor who will help you with the terms of business that must be signed to enter your property into the Auction. Your solicitor will also help in preparing the contract of sale and supporting legal paperwork. Again, a good Auctioneer will liaise with your solicitor fully, ensuring everything is in place for the Auction sale. We invite you to talk to TTPL for a full conveyancing service.</p>
                        </div>
                        <div class="col-md-6">
                            <p>Once the marketing of your property begins, the Auctioneer may receive offers on it before the Auction. We will tell you straightaway. Sometimes unrealistic offers are made and we will arrange with you to advise only when the offer is significant. If your property is sold before Auction, it will be withdrawn once contracts are exchanged.</p>
                            <p>A lot of people are put off from selling at Auction because they’re worried they will lose money if bids are very low. Don’t worry; Andrew Kelly Auctions will talk to you about setting a reserve price, which is the minimum price you want your home to be sold for. This is normally discussed, in principle, when you instruct us. It safeguards you from setting a reserve that is too low or too high. It will be confirmed two days before the sale. It’s confidential and will not be disclosed to anyone.</p>
                            <p>Andrew Kelly Auctions will provide you with a free, no obligation Auction appraisal of your property. We will advise you on a recommended reserve and give you an idea of the price that you could achieve at Auction.</p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    
    @include('frontend.partials.testimonials')

    <!-- Selling Advice -->
    <section class="section article">
        <div class="container">
            <div class="row">
                <header class="section-header col-md-12">
                    <h2 class="section-heading">A Guide For Buyers</h2>
                    <hr>
                </header>

                <article class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <p>People buy at Auction for lots of reasons. You may want to move quickly or be looking for a plot of land to build on, a repossessed property to improve, or quite simply you may want a quick purchase. You may want something really unusual or just want to find a home. Also, many buy-to-let investors buy at Auction as the bigger the sale, the greater the number of opportunities. It doesn’t matter what your reason for buying at auction is, the key is preparation. It is not complicated but can appear daunting, that is where Andrew Kelly Auctions can help.</p>
                            <p>First you must decide what sort of property you really want. It is a good idea to make a list of “must haves” and “nice to haves”. Must you have 3 bedrooms, would you like a conservatory? Must you have off-street parking, would you like a large garden? So where should you look?</p>
                            <p>Registering with Andrew Kelly Auctions will ensure that you are first to hear about new opportunities coming to the market. The quickest way to get a copy of our Auction catalogue is to subscribe to our mailing list, this way, it will be sent to you as soon as it is produced.</p>
                            <p>Once you’ve found the property you’d like to buy, here’s a list of the things you need to do beforehand:<br/>
                                View it more than once. Arrange your finance. If you are financing the purchase through a mortgage you should have a formal offer before the Auction. Make sure you have a survey. You will probably need this if you are financing the purchase with a mortgage. If you are paying “cash” you’ll want to make sure the property is structurally sound and isn’t going to drain your resources. You’ll need a deposit, normally 10% of the purchase price, which is payable at the Auction once you’ve bought the property. Do remember the legal documentation charge also, typically £335 including VAT and Obtain a legal pack (£20.00 including VAT).</p>
                            <p>Consult a solicitor about the purchase to avoid any legal pitfalls; we can, of course help through TTPL. Have a look at a copy of the legal documents from the Auctioneer; make sure you have a copy of the Auction catalogue as this forms part of the legal contract between you and the seller. Some of these processes will cost money, but it’s worth it for your peace of mind. Andrew Kelly Auctions will be able to guide you as to likely cost. Having done your homework and you feel you can’t wait, you can submit an offer before the Auction which, if suitable, is forwarded to the seller. If this is accepted, your purchase will go ahead and when contracts have been exchanged, the property will be withdrawn from the Auction. You will have to move quickly!</p>
                            <p><strong>At the Auction:</strong></p>
                            <p>By far the most exciting part of buying or selling at Auction is the sale itself. Be prepared. Do check that the property that you are after is still available. At the Sale you’ll need two </p>
                        </div>
                        <div class="col-md-6">
                            <p>forms of identification, your auction catalogue, your solicitor’s details, your deposit and your legal documentation charge. If you are successful you will need to pay both when contracts are exchanged which happens on the fall of the hammer. Under no circumstances will the Auctioneers accept cash. Don’t forget buildings insurance, it’s your responsibility from the fall of the hammer.</p>
                            <p>Before the sale begins the Auctioneer will check that everybody has a copy of the ‘Addendum or Announcement’ sheet and will read out any last minute alterations to the catalogue details. He will not accept any further questions at this stage. When the property you are interested in comes up, make sure you know the maximum price you can afford to pay - it’s easy to get carried away! Once bidding begins, potential buyers will be asked to make their bids clearly either by raising their hand, a copy of the Auction catalogue or their bidding number. Don’t worry; you can’t buy a property by scratching your head! Once the final bid has been put forward the Auctioneer will make it perfectly clear that the property is to be sold.</p>
                            <p>If a Lot fails to reach the reserve price, it will be withdrawn from the Auction and the Auctioneer will invite anyone still interested to talk afterwards. Sales can still be made if a price is agreed with the seller.</p>
                            <p>If you can’t attend then you can arrange for someone to submit bids on your behalf. They’ll need your written consent and you should agree limits before bids are submitted. If you want someone from the Auction team to do this you should contact them at least two days before the Auction.</p>
                            <p>You can also submit telephone bids to a member of the Auction team who’ll be in the Auction room. They’ll then make bids on your behalf. However, attending the sale is by far the best way to bid. If you bid by phone you’ll miss out on the exciting part of your purchase. A member of our staff is always at hand to help you. Once the hammer falls, contracts are legally exchanged. The contract is signed and the deposit handed over immediately. The Auctioneer is legally entitled to sign the contract on behalf of both the buyer and seller if necessary. The deal is done and you now have a new home.</p>
                            <p>The completion of the sale takes place anything between 14 and 28 days later; this will be stated in the general conditions of sale in the Auction catalogue or as a special condition of sale in the legal documents. The balance of the sale monies will be paid on completion. A solicitor will normally deal with this part of the sale.</p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>


    @include('frontend.partials.accreditations')

@stop