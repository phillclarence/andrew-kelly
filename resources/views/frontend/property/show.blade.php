@extends('frontend.master')

@section('title', $property->advert_heading . ' - ')

@section('scripts')
    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBzfwFqSpk6NH7Oa9tgx0rvUbFTYSsOt_s'></script>
@endsection

@section('content')

    <!-- Property Search -->
    <section class="search property">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('frontend.partials.property-search')
                </div>
            </div>
        </div>
    </section>

    <div class="search-toggle">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="title">Search Filter</span>
                    <span class="search-toggle-trigger"><i class="fa fa-chevron-down"></i></span>
                </div>
            </div>
        </div>
    </div>

    <section class="section PropertyListing">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    @if($property->pictures)
                        <div id="slider" class="PropertyListing__slider flexslider">
                            @if ($property->isSold())
                                <span class="tag is-sold in-corner">Sold STC</span>
                            @endif

                            <ul class="slides">
                                @foreach($property->pictures as $picture)
                                    <li>
                                        <img src="{!! $picture->filename !!}" alt="{!! $picture->name !!}">
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div id="carousel" class="PropertyListing__carousel flexslider">
                            <ul class="slides">
                                @foreach($property->pictures as $picture)
                                    <li>
                                        <img src="{!! $picture->filename !!}" alt="{!! $picture->name !!}">
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <header class="PropertyListing__header">
                        <h1 class="PropertyListing__heading">{!! $property->street !!}</h1>
                        <span class="PropertyListing__subheading">{!! $property->district !!}</span>
                        <span class="PropertyListing__subheading">{!! $property->town !!}</span>

                        <div class="PropertyListing__pricing">
                            <span class="PropertyListing__price">{!! $property->price !!}</span>
                            <span class="tag {{ $property->status }}">{!! $property->priority !!}</span>
                        </div>

                    </header>
                </div>
            </div>

            <div id="property-details" class="row">
                <div class="col-md-12">
                    <h2 class="PropertyListing__h2">Property</h2>

                    <p><strong>{!! $property->advert_heading !!}</strong></p>
                    <p>{!! $property->main_advert !!}</p>

                    <h2 class="PropertyListing__h2">Details</h2>

                    @if($property->rooms)
                        @foreach($property->rooms as $room)
                            <h3 class="Room__heading">{!! $room->name !!}</h3>
                            <span class="Room__measurement">{!! $room->measurement_text !!}</span>
                            <p class="Room__description">{!! $room->description !!}</p>
                        @endforeach
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:history.back()" class="ui-button ui-button--back utility--mt"><i class="fa fa-chevron-left"></i> Back to Properties</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section callouts grey">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    @include('frontend.partials.callouts.valuation', ['description' => false])
                </div>
                <div class="col-md-4">
                    @include('frontend.partials.callouts.viewing', ['description' => false])
                </div>
                <div class="col-md-4">
                    @include('frontend.partials.callouts.full-details', ['description' => false])
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    @include('frontend.partials.callouts.property.map', ['property' => $property->property_reference])
                </div>
                <div class="col-md-4">
                    @include('frontend.partials.callouts.property.street-view', ['property' => $property->property_reference])
                </div>
                <div class="col-md-4">
                    @include('frontend.partials.callouts.property.floorplans', ['property' => $property->property_reference])
                </div>
            </div>
        </div>
    </section>

    @include('frontend.partials.accreditations')

@stop