@extends('frontend.master')

@section('title', $post->post_title . ' - ')

@section('content')

    <section class="section posts">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    @if($post)
                        <h1>{!! $post->post_title !!}</h1>

                        <article>{!! $post->post_content !!}</article>
                    @else

                    @endif
                </div>

                <aside class="col-md-3">
                    @include('frontend.blog.partials.sidebar')
                </aside>
            </div>
        </div>
    </section>

    @include('frontend.partials.accreditations')

@stop