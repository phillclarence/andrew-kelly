<div class="sidebar">
    <header class="section-header">
        <h3 class="section-heading">Categories</h3>
        <hr>
    </header>
    <ul>
        @foreach ($categories as $category)
            <li><a href="{!! route('blog.category', $category->term->slug) !!}">{!! $category->term->name !!}</a></li>
        @endforeach
    </ul>
</div>