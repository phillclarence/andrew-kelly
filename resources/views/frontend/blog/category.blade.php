@extends('frontend.master')

@section('title', $category->name . ' - ')

@section('content')

    <section class="section posts">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <header class="section-header ">
                        <h1 class="section-heading">{!! $category->name !!}</h1>
                        <hr>
                    </header>
                    <div class="row">
                        @foreach($category->posts as $post)
                            <div class="col-md-6">
                                <article class="card">

                                    <a href="{!! route('blog.single', [$category->slug, $post->slug]) !!}">
                                        @if(isset($post->thumbnail->attachment))
                                            <img class="card-img" src="{!! asset($post->thumbnail->attachment->url) !!}" alt="">
                                        @else
                                            <img class="card-img" src="{!! asset('images/placeholder.png') !!}" alt="Image coming soon">
                                        @endif
                                    </a>

                                    <div class="card-body">
                                        <a href="{!! route('blog.single', [$category->slug, $post->slug]) !!}">
                                            <h3 class="card-heading">{!! $post->post_title !!}</h3>
                                        </a>

                                        <a href="{!! route('blog.single', [$category->slug, $post->slug]) !!}" class="card-read-more">Continue reading</a>
                                    </div>
                                </article>
                            </div>
                        @endforeach
                    </div>
                </div>

                <aside class="col-md-3">
                    @include('frontend.blog.partials.sidebar')
                </aside>
            </div>
        </div>
    </section>

    @include('frontend.partials.accreditations')

@stop