@extends('frontend.master')

@section('title', 'Foundation - ')

@section('content')

	@include('frontend.partials.masthead')
	
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h1 class="section-heading">Foundation</h1>
					<hr>
				</header>

				<article class="col-md-6">
					<p>The Foundation was formed in July 2007 and registered with the Charities Commission (reg 1120602) as a U.K. Charity in August 2007.</p>
					<p>It’s main aim is to raise funds to provide much needed financial support to a whole range of mainly local charities and deserving causes with an emphasis on children and young people.</p>
					<p>The Foundation itself was born out of several years successful fund raising by Andrew Kelly & Associates mainly through an annual Charity Dinner where local business people and other generous supporters raised money to support U.K. Charities such as Whizz Kidz (reg 802872) - providing specialised mobility aids and help for severely disabled children; Touching Tiny Lives (reg 20871) - an appeal to raise funds for vital medical research to prevent premature births and reduce the death rate of premature babies; and SCOPE (reg 208231) - the charity that provides vital support to people born with Cerebral Palsy - and their families.</p>
					<p>Staff at Andrew Kelly & Associates had also raised funds for those charities via their earnings, completing sponsored marathons, and sponsored high altitude climbs – amongst other things.</p>
					<p>The Foundation was formed as a result of this activity and the decision made to focus more of its support closer to home - mainly charities in the greater Rochdale and Rossendale areas with an emphasis on children and young people in the locality.</p>
				</article>

				<article class="col-md-6">
					<p>The Foundation intends to continue to host this dinner annually as its main fund-raising event along with of course, other events and fund-raising ideas, throughout the calendar.</p>
					<p>There are three Trustees of the Foundation:<br/>
						Jim Burns - Chairman<br/>
						Andrew Kelly<br/>
						Angela McFarlane - Secretary<br/></p>
					<p>If you require any further information about the Foundation itself, the work we do, or indeed want to offer support - financially or practically, please do not hesitate to contact any of the Trustees at any time.</p>
					<p>The Andrew Kelly Foundation (reg no. 1120602).<br/>
						Registered Office: 30 Market Place, Heywood OL10 4NL<br/>

						Tel: 01706 623880<br/>
						Fax: 01706 366066</p>
				</article>
			</div>
		</div>
	</section>

	<section class="section services grey">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.overseas')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.careers')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.foundation')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.epc')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.conveyancing')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.probate')
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.partials.accreditations')

@stop