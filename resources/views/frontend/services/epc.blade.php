@extends('frontend.master')

@section('title', 'EPC - ')

@section('content')

	@include('frontend.partials.masthead')
	
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h1 class="section-heading">EPC's</h1>
					<hr>
				</header>

				<article class="col-md-6">
					<p>Andrew Kelly & Associates have trained professionals who can provide you with all the information you need about Energy Performance Certificates (EPCs). We have qualified Domestic Energy Assessors who will visit your home to undertake a full assessment of the property. Our professionals will be on hand to explain the “in and outs” of the process and discuss energy ratings and recommendations where appropriate.</p>
					<p>By choosing Andrew Kelly EPCs as your EPC provider you are guaranteed a personal service. There is no “outsourcing” to external companies. We control the quality and production of your EPC from start to finish providing an efficient, friendly and experienced professional who will be able to provide you with advice and answer your questions on all aspects of the EPC.</p>
					<p>Producing the EPC is a natural extension of the Andrew Kelly marketing process and therefore benefits from the Andrew Kelly Group’s commitment to high levels of customer service. The process is more efficient and easier to control when we deal with all of the tasks that must be completed prior to marketing.</p>
					<p>An Energy Performance Certificate (EPC) is required for all homes whenever built, rented or sold. If you are buying or selling a home it is now law to have a certificate. They are also required on construction of new homes and  are needed for rented homes the first time the property is let after 1 October 2008.</p>
					<p>The certificate records how energy efficient a property is as a building and provides A-G ratings. These are similar to the labels now provided with domestic appliances such as refrigerators and washing machines.</p>
					<p>They are produced using standard methods and assumptions about energy usage so that the energy efficiency of one building can easily be compared with another building of the same type. This allows prospective buyers, tenants, owners, occupiers and purchasers to see information on the energy efficiency and carbon emissions from their building so they can consider energy efficiency and fuel costs as part of their investment.</p>
					<p>An EPC is always accompanied by a recommendation report that lists cost effective and other measures (such as low and zero carbon generating systems) to improve the energy rating. A rating is also given showing what could be achieved if all the recommendations were implemented.</p>
					<p>The certificate is important because nearly 50 per cent of the UK's energy consumption and carbon emissions arise from the way our buildings are lit, heated and used. Even comparatively minor changes in energy performance and the way we use each building will have a significant effect in reducing energy consumption.</p>
				</article>

				<article class="col-md-6">
					<p>On 14th February 2007, the Government introduced legislation stating that every home placed on the market for sale (with few exceptions) must have a HIP. It was supposed to bring together valuable information at the start of the property sale process such as a sale statement, local searches, evidence of title, title plans and an EPC.</p>
					<p>Due to several limitations, the cost, and time delays in marketing a home, HIPs were widely discredited and on the 21st May 2010 HIPs were suspended by the Government. In an attempt to meet European Energy Reduction targets, the Energy Performance Certificate (EPC) element of the HIP was retained.</p>
					<p>The energy-efficiency rating is a measure of a home's overall efficiency. The higher the rating, the more energy-efficient the home is, and the lower the fuel bills are likely to be.</p>
					<p>The environmental impact rating is a measure of a home's impact on the environment in terms of carbon dioxide (CO2) emissions - the higher the rating, the less impact it has on the environment.</p>
					<p>Each rating is based on the performance of the building itself and its services (such as heating and lighting), rather than the domestic appliances within it. This is known as an asset rating. The certificate also lists the potential rating of the building if all the cost-effective measures were installed.</p>
					<p>The ratings will vary according to the age, location, size and condition of the building. The potential rating on the certificate will take these factors into account, and the suggested measures will be tailored so that they are realistic for the particular building.</p>
					<p>The certificate also includes a recommendation report, providing information about ways to improve the energy performance of the property. Recommendations include cost effective improvements and further improvements (that achieve higher standards but are not necessarily cost effective). For each improvement the level of cost, typical cost savings per year and the performance rating after improvement are listed. The potential rating shown on the certificate is based on all the cost effective recommendations being implemented.</p>
				</article>
			</div>
		</div>
	</section>

	<section class="section services grey">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.overseas')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.careers')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.foundation')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.epc')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.conveyancing')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.probate')
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.partials.accreditations')

@stop