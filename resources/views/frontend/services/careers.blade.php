@extends('frontend.master')

@section('title', 'Careers - ')

@section('content')

	@include('frontend.partials.masthead')
	
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h1 class="section-heading">Careers</h1>
					<hr>
				</header>

				<article class="col-md-12">
					<p>If you are looking for a new challenge, contact the main Andrew Kelly number and ask about how you can apply to join our expanding team, experience isn’t necessary as training will be given.</p>
				</article>
			</div>
		</div>
	</section>

	<section class="section services grey">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.overseas')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.careers')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.foundation')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.epc')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.conveyancing')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.probate')
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.partials.accreditations')

@stop