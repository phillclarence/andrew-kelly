@extends('frontend.master')

@section('title', 'Conveyancing - ')

@section('content')

	@include('frontend.partials.masthead')
	
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h1 class="section-heading">Conveyancing</h1>
					<hr>
				</header>

				<article class="col-md-6">
					<p>Transfer Property Lawyers are the preferred conveyancers of Andrew Kelly & Associates, they will guide you through each step of the buying or selling process from start to finish. They will keep you informed of what needs to be done and when. Contact them and they will provide you with an itemised quotation for your sale or purchase.</p>
					<p>TTPL are an innovative company using cutting edge technology to provide you with an effective and efficient service but with a focus on old fashioned values and courtesy. With the latest technology they can provide you with information on your property transaction by email and text messaging as well as the usual telephone calls. TTPL provide a friendly and flexible service for buying or selling your house. They have a team of approachable and friendly professionals who will guide you through the process with ease and efficiency. Their clients are delighted with their service, in fact 100% of their clients would recommend them to family or friends.</p>
					<p>At TTPL they appreciate that all their clients require peace of mind in relation to potential legal costs when a transaction becomes protracted and more complicated than originally expected. This is why the fees that are quoted in initial correspondence are the fees that you will pay on completion of your transaction, regardless of the extent of the work involved.</p>
				</article>

				<article class="col-md-6">
					<p>TTPL will take you through each step of the Conveyancing Process, at a price that is agreed in advance with you, in plain terms designed to keep you up to speed without baffling you with jargon.</p>
					<p>TTPL pride themselves in providing a friendly service in a relaxed and accommodating environment. When surveyed, their clients have been happy and delighted with the quality of professional service they received from TTPL.</p>
					<p>Why not contact Title Transfer for an informal discussion on 0845 094 3358 or email them at <a href="mailto:enquiries@ttpl.co.uk">enquiries@ttpl.co.uk</a></p>
				</article>
			</div>
		</div>
	</section>

	<section class="section services grey">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.overseas')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.careers')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.foundation')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.epc')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.conveyancing')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.probate')
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.partials.accreditations')

@stop