@extends('frontend.master')

@section('title', 'Overseas - ')

@section('content')

	@include('frontend.partials.masthead')
	
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h1 class="section-heading">Overseas</h1>
					<hr>
				</header>

				<article class="col-md-6">
					<p>Andrew Kelly now offer Spanish Properties, we have a large selection of new and resale properties available.</p>
					<p>For the UK Citizens, Spain has become the number one holiday destination. Every year, more and more UK Citizens people are discovering the delights of Spain for themselves. It is not surprising that before long they dream of owning their very own home in Spain.</p>
					<p>Spain offers all the ingredients for a perfect way of life. A climate that boasts of around 320 days of sunshine every year. Beautiful sandy beaches, sparkling blue sea and a history steeped in culture. Hot summers and a low humidity with cooling sea breezes, mild winters with temperatures of around 17°C. Good reason for many people to choose to live in Spain. In southern Spain the Costa Blanca, 'the White Coast', so called because of its long stretches of sandy beaches, has become the most sought after location, where many people have realised their dream and own their own home.</p>
					<p><strong>Why the Costa Blanca?</strong></p>
					<p>Following a recent investigation, the World Health Organisation has stated that the Costa Blanca has the most beneficial climate in the whole of Europe, which is why this area is especially popular with people of retirement age. Many younger people and families also decide to come to this area for their immediate and future benefit.</p>
				</article>

				<article class="col-md-6">
					<p>Andrew Kelly works in the interest of you, the customer. Besides helping you to select the house of your dreams, we will be on hand to advise you through all the legal and financial aspects connected with your purchase, whether you intend to stay temporarily or permanently in Spain.</p>
					<p>After you have studied all the information at home, we can arrange an inspection trip for you to see for yourself the range of properties that are available on the Costa Blanca. During your stay we accompany you on your visit and are on hand to advise you and answer any queries you may have. Once you have found the right investment, you can leave it to us to make all the necessary arrangements.</p>
					<p>You can be reassured that even after your purchase has been completed, we are still available should you require any help or advice. You can be certain that your purchase/investment will bring you maximum enjoyment and relaxation, from start to finish. Our personal service and guidance will guarantee you that. Also should you wish, we can be of service with regard to furnishings, removals, insurances, connections and contracts for mains services ie electricity, water and telephone. If you intend to rent your property we can help you with that aspect also.</p>
					<p>We arrange and supervise the buying and selling of exclusive Villas, Apartments, Bungalows, Chalets, Fincas and commercial premises, land and other such projects on the Costa Blanca.</p>
				</article>
			</div>
		</div>
	</section>

	<section class="section services grey">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.overseas')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.careers')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.foundation')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.epc')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.conveyancing')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.probate')
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.partials.accreditations')

@stop