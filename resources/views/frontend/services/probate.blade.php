@extends('frontend.master')

@section('title', 'Probate - ')

@section('content')

	@include('frontend.partials.masthead')
	
	<section class="section article">
		<div class="container">
			<div class="row">
				<header class="section-header col-md-12">
					<h1 class="section-heading">Probate</h1>
					<hr>
				</header>

				<article class="col-md-12">
					<p>The death of a loved one is a painful and traumatic experience and having to manage the practicalities is usually the last thing you will want to think about. Here at Andrew Kelly Probate, we offer a service to manage all of the practicalities so that you have only one port of call. We can take away the stress of dealing with such things as;</p>
					<ul>
						<li>House clearance</li>
						<li>Central Heating and Water drain down</li>
						<li>Meter readings</li>
						<li>Probate and Property Valuations</li>
						<li>Organising the solicitor to deal with the Probate</li>
					</ul>
					<p>Call us on 01706 831013 click the contact button to make an enquiry.</p>
				</article>
			</div>
		</div>
	</section>

	<section class="section services grey">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.overseas')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.careers')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.foundation')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.epc')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.conveyancing')
					</div>
				</div>

				<div class="col-md-4">
					<div class="callout">
						@include('frontend.partials.callouts.services.probate')
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.partials.accreditations')

@stop