@extends('frontend.master')

@section('content')
	
	@include('frontend.partials.masthead')

	@include('frontend.partials.callouts', ['class' => '', 'type' => ''])

	@include('frontend.partials.featured-properties')

	{{--@include('frontend.partials.agents')--}}

	@include('frontend.partials.testimonials')

	@include('frontend.partials.blog')

	@include('frontend.partials.accreditations')

@stop