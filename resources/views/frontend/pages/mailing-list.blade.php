@extends('frontend.master')

@section('content')

    @include('frontend.partials.masthead')

    <section class="section contact">
        <div class="container">
            <div class="row">
                <aside class="col-md-3">
                    <header class="section-header">
                        <h2 class="section-heading">Information</h2>
                        <hr>
                        <p>Please complete the form to be on the Andrew Kelly sales mailing list which we use to keep you updated with the types of property you are interested in. If you are interested on being kept up to date with rental properties then please ring 01706 352266</p>
                    </header>
                </aside>

                <div class="col-md-8  col-md-offset-1">
                    @include('frontend.partials.forms.mailing-list')
                </div>
            </div>
        </div>
    </section>

    @include('frontend.partials.callouts', ['class' => 'grey'])

    @include('frontend.partials.accreditations')

@stop