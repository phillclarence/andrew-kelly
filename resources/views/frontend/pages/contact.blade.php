@extends('frontend.master')

@section('title', 'Contact ')

@section('content')
	
	<section class="section contact">
		<div class="container">
			<div class="row">
				<aside class="col-md-3">
					<header class="section-header">
						<h2 class="section-heading">Branches</h2>
						<hr>
					</header>

					<article class="branches">
						<p><strong>Rochdale &amp; Norden Office</strong><br>
							Sales: 01706 350363<br>
							Lettings: 01706 352266<br>
						</p>
						<p><strong>Heywood Office</strong><br/>Sales: 01706 369911</p>
						<p><strong>Littleborough Office</strong><br/>Sales: 01706 372225</p>
						<p><strong>Milnrow Office</strong><br/>Sales: 01706 861010</p>
						<p><strong>Sales lines are open 24 hours<br/> a day, 7 days a week.</strong></p>
					</article>
				</aside>

				<div class="col-md-8 col-md-offset-1">
					<header class="section-header">
						<h2 class="section-heading">Leave a Message</h2>
						<hr>
					</header>

					@include('frontend.partials.forms.contact-form')
				</div>				
			</div>
		</div>
	</section>

	@include('frontend.partials.accreditations')

	<section class="section branch-details">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<header class="section-header">
						<h2 class="section-heading">Branches</h2>
						<hr>
					</header>
				</div>

				<div class="col-md-6">
					<div class="branch">
						<strong>Rochdale & Norden Office</strong>
						<p>124 Yorkshire Street, Rochdale, Lancashire OL16 1LA</p>
						<p><strong>Sales</strong><br/>
							Tel: 01706 350363<br/>
							Email: <a href="mailto:rochdale@andrew-kelly.co.uk">rochdale@andrew-kelly.co.uk</a>
						</p>
						<p><strong>Lettings</strong><br/>
							Tel: 01706 352266<br/>
							Email: <a href="mailto:lettings@andrew-kelly.co.uk">lettings@andrew-kelly.co.uk</a>
						</p>
					</div>
					<div class="branch">
						<strong>Littleborough Office</strong>
						<p>24/26 Hare Hill Road, Littleborough, Lancashire OL15 9BA<br/>
						Tel: 01706 372225<br/>Email: <a href="mailto:littleborough@andrew-kelly.co.uk">littleborough@andrew-kelly.co.uk</a></p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="branch">
						<strong>Milnrow Office</strong>
						<p>112 Dale Street, Milnrow, Lancashire OL16 4HX<br/>
						Tel: 01706 861010<br/>Email: <a href="mailto:milnrow@andrew-kelly.co.uk">milnrow@andrew-kelly.co.uk</a></p>
					</div>
					<div class="branch">
						<strong>Heywood Office</strong>
						<p>30 Market Place, Heywood, Lancashire OL10 4NL<br/>
						Tel: 01706 369911<br/>Email: <a href="mailto:heywood@andrew-kelly.co.uk">heywood@andrew-kelly.co.uk</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>

@stop