<section id="property-search" class="property-search">
	{!! Form::open(['route' => 'properties', 'method' => 'GET', 'id' => 'property-search']) !!}
		<input type="hidden" name="sort" value="{!! request()->get('sort') !!}" >
		<input id="previous-type" type="hidden" name="previous-type" value="{!! request()->get('type') !!}" >
		<input id="property-type" type="hidden" name="type" value="{!! request()->get('type') !!}" >
		<input id="lat" type="hidden" name="lat" value="{!! request()->get('lat') !!}">
		<input id="lng" type="hidden" name="lng" value="{!! request()->get('lng') !!}">

		<header>
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<span class="title">Property Search</span>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="property-type">
						<a href="#" data-type="sales">Sales</a>
						<a href="#" data-type="lettings">Lettings</a>
						<a href="#" data-type="auctions">Auctions</a>
					</div>
				</div>
			</div>
		</header>
		<main>
			<div class="row">
				<div class="col-md-3">
					<input id="area" type="text" name="area" value="{!! request()->get('area') !!}" placeholder="Area">

					<div class="ui-select">
						{!! Form::select('bedrooms', $bedrooms, request()->input('bedrooms')) !!}
					</div>
				</div>
				<div class="col-md-3">
					<input type="text" name="keyword" value="" placeholder="Keyword">

					<div class="ui-select">
						{!! Form::select('minPrice', $minPrice, request()->input('minPrice')) !!}
						{!! Form::select('minRent', $minRent, request()->input('minRent'), ['style' => 'display:none;']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="ui-select">
						{!! Form::select('propertyType', $propertyTypes, request()->input('propertyType')) !!}
					</div>

					<div class="ui-select">
						{!! Form::select('maxPrice', $maxPrice, request()->input('maxPrice')) !!}
						{!! Form::select('maxRent', $maxRent, request()->input('maxRent'), ['style' => 'display:none;']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="ui-select">
						{!! Form::select('bathrooms', $bathrooms, request()->input('bathrooms')) !!}
					</div>

					<button class="ui-button search"><i class="fa fa-search"></i> Search</button>
				</div>
			</div>
		</main>
	{!! Form::close() !!}
</section>