<div class="callout">
	<figure class="callout-icon">
		<a href="#property-details">
			<object width="140" height="140" data="{!! asset('images/icons/icon-notes.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Full Details</h3>
		</a>
	</figure>

	<hr>
	@if(isset($description) && $description)
		<p class="callout-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis.</p>
	@endif
</div>