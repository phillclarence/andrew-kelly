<div class="callout">
    <figure class="callout-icon">
        <a href="#" class="modal-trigger" data-modal="street-view" data-property="{!! $property !!}">
            <object width="140" height="140" data="{!! asset('images/icons/icon-location.svg') !!}" type="image/svg+xml">
            </object>

            <h3 class="callout-heading">Street View</h3>
        </a>
    </figure>

    <hr>
</div>