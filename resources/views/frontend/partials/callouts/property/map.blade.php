<div class="callout">
    <figure class="callout-icon">
        <a href="#" class="modal-trigger" data-modal="map" data-property="{!! $property !!}">
            <object width="140" height="140" data="{!! asset('images/icons/icon-globe.svg') !!}" type="image/svg+xml">
            </object>

            <h3 class="callout-heading">Map</h3>
        </a>
    </figure>

    <hr>
</div>