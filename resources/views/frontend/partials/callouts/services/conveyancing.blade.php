<div class="callout">
	<figure class="callout-icon">
		<a href="{!! route('services.show', 'conveyancing') !!}">
			<object width="140" height="140" data="{!! asset('images/icons/icon-search.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Conveyancing</h3>
		</a>
	</figure>

	<hr>
	<p class="callout-desc">Transfer Property Lawyers are the preferred conveyancers of Andrew Kelly & Associates, they will guide you through each step of the buying or selling process from start to finish.</p>
</div>