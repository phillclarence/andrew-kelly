<div class="callout">
	<figure class="callout-icon">
		<a href="{!! route('services.show', 'overseas') !!}">
			<object width="140" height="140" data="{!! asset('images/icons/icon-coins.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Overseas</h3>
		</a>
	</figure>

	<hr>
	<p class="callout-desc">Andrew Kelly now offer Spanish Properties, we have a large selection of new and resale properties available.</p>
</div>