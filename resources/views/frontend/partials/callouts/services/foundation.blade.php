<div class="callout">
	<figure class="callout-icon">
		<a href="{!! route('services.show', 'foundation') !!}">
			<object width="140" height="140" data="{!! asset('images/icons/icon-coins.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Foundation</h3>
		</a>
	</figure>

	<hr>
	<p class="callout-desc">The Andrew Kelly Foundation was formed to provide financial support to mainly local charities and deserving causes with an emphasis on children and young people.</p>
</div>