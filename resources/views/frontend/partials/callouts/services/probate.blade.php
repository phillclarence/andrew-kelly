<div class="callout">
	<figure class="callout-icon">
		<a href="{!! route('services.show', 'probate') !!}">
			<object width="140" height="140" data="{!! asset('images/icons/icon-vault.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Probate</h3>
		</a>
	</figure>

	<hr>
	<p class="callout-desc">The death of a loved one is a painful experience, here at Andrew Kelly Probate, we offer a service to manage all of the practicalities so that you have only one port of call.</p>
</div>