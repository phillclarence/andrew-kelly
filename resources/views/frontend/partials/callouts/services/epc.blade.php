<div class="callout">
	<figure class="callout-icon">
		<a href="{!! route('services.show', 'epc') !!}">
			<object width="140" height="140" data="{!! asset('images/icons/icon-lightbulb.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">EPC's</h3>
		</a>
	</figure>

	<hr>
	<p class="callout-desc">An Energy Performance Certificate (EPC) is required for all homes whenever built, rented or sold. If you are buying or selling a home it is now law to have a certificate.</p>
</div>