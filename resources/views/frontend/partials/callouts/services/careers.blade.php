<div class="callout">
	<figure class="callout-icon">
		<a href="{!! route('services.show', 'careers') !!}">
			<object width="140" height="140" data="{!! asset('images/icons/icon-hat.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Careers</h3>
		</a>
	</figure>

	<hr>
	<p class="callout-desc">Are you looking for a new challenge? Andrew Kelly are looking for highly motivated indiviuals to join our growing team.</p>
</div>