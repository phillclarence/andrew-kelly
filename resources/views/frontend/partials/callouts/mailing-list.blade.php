<div class="callout">
	<figure class="callout-icon">
		<a href="{!! route('mailing') !!}">
			<object width="140" height="140" data="{!! asset('images/icons/icon-notes.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Mailing List</h3>
		</a>
	</figure>

	<hr>
	@if(isset($description) && $description)
		<p class="callout-desc">Do you know exactly what you’re looking for? The area you are looking to move into? Let the team at Andrew Kelly know and we will work closely with you to help you achieve this with any new properties coming to the market.</p>
	@endif
</div>