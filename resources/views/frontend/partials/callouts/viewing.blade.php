<div class="callout">
	<figure class="callout-icon">
		<a href="#" class="modal-trigger" data-modal="viewing">
			<object width="140" height="140" data="{!! asset('images/icons/icon-calendar.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Arrange a Viewing</h3>
		</a>
	</figure>

	<hr>

	@if(isset($description) && $description)
		<p class="callout-desc">Have you seen one of our properties and would like to take a closer look? Click here to leave your details and what times you are available to arrange a viewing of the house that might just be your perfect fit.</p>
	@endif
</div>