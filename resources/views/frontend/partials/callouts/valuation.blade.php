<div class="callout">
	<figure class="callout-icon">
		<a href="#" class="modal-trigger" data-modal="valuation">
			<object width="140" height="140" data="{!! asset('images/icons/icon-tag.svg') !!}" type="image/svg+xml">
			</object>
		
			<h3 class="callout-heading">Book a Valuation</h3>
		</a>
	</figure>

	<hr>

	@if(isset($description) && $description)
		<p class="callout-desc">Are you looking to sell or rent out your property? Find out exactly how much your property is worth on today’s property market to either sell or rent with our free no obligation valuation.</p>
	@endif
</div>