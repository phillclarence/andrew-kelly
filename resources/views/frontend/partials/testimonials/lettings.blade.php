<section class="section testimonials">
	<div class="container">
		<div class="row">
			<header class="section-header col-md-12">
				<h2 class="section-heading">Client Comments</h2>
				<hr>
			</header>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="testimonials-carousel">
					@foreach ($testimonials as $testimonial)
						<div class="testimonial">
							<blockquote>{!! $testimonial->post_content !!}</blockquote>
							<p class="client">{!! $testimonial->post_title !!}</p>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>