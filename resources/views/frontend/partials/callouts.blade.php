<section class="section callouts {!! $class !!}">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				@include('frontend.partials.callouts.valuation', ['description' => true])
			</div>
			<div class="col-md-4">
				@include('frontend.partials.callouts.viewing', ['description' => true])
			</div>
			<div class="col-md-4">
				@include('frontend.partials.callouts.mailing-list', ['description' => true])
			</div>
		</div>
	</div>
</section>