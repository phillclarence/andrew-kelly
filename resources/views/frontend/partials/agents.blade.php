<section class="section agents">
	<div class="container">
		<div class="row">
			<header class="section-header col-md-12">
				<h2 class="section-heading">Meet Our Agents</h2>
				<hr>
			</header>
		</div>

		<div class="row">
			<div class="card-carousel agents">
				<article class="card agent">
					<img src="{!! asset('media/agents/placeholder.jpg') !!}" alt="">

					<div class="card-body">
						<h4 class="card-heading">ANDREA HOPE</h4>
						<p class="card-sub">Sales agent</p>
					</div>
				</article>

				<article class="card agent">
					<img src="{!! asset('media/agents/placeholder.jpg') !!}" alt="">

					<div class="card-body">
						<h4 class="card-heading">ANDREA HOPE</h4>
						<p class="card-sub">Sales agent</p>
					</div>
				</article>

				<article class="card agent">
					<img src="{!! asset('media/agents/placeholder.jpg') !!}" alt="">

					<div class="card-body">
						<h4 class="card-heading">ANDREA HOPE</h4>
						<p class="card-sub">Sales agent</p>
					</div>
				</article>

				<article class="card agent">
					<img src="{!! asset('media/agents/placeholder.jpg') !!}" alt="">

					<div class="card-body">
						<h4 class="card-heading">ANDREA HOPE</h4>
						<p class="card-sub">Sales agent</p>
					</div>
				</article>

				<article class="card agent">
					<img src="{!! asset('media/agents/placeholder.jpg') !!}" alt="">

					<div class="card-body">
						<h4 class="card-heading">ANDREA HOPE</h4>
						<p class="card-sub">Sales agent</p>
					</div>
				</article>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<nav class="card-carousel-nav">
					<button class="prev" data-carousel="agents"><i class="fa fa-chevron-left"></i></button>
					<button class="next" data-carousel="agents"><i class="fa fa-chevron-right"></i></button>
				</nav>
			</div>
		</div>
	</div>
</section>