<section class="section featured-properties">
	<div class="container">
		<div class="row">
			<header class="section-header col-md-12">
				<h2 class="section-heading">Latest Properties</h2>
				<hr>
			</header>
		</div>

		<div class="row">
			<div class="card-carousel properties">
				@foreach($properties as $property)
					<div class="card Property">
						<a href="{!! route('properties.show', $property->property_reference) !!}">
							<figure class="Property__image-wrapper">
								@if($property->pictures->count())
									<img class="Property__image" src="{!! $property->pictures[0]->filename !!}" alt="{!! $property->property_reference !!}">
								@else
									<img class="Property__image" src="{!! asset('images/placeholder.png') !!}" alt="Image coming soon">
								@endif
							</figure>
							<footer class="Property__footer">
								<h3 class="Property__heading">{!! $property->street !!}</h3>
								<span class="Property__subheading">{!! $property->district !!}</span>
								<span class="Property__subheading">{!! $property->town !!}</span>
								<p class="Property__price">{!! $property->price !!}</p>
							</footer>
						</a>
					</div>
				@endforeach
			</div>	
		</div>

		<div class="row">
			<div class="col-md-12">
				<nav class="card-carousel-nav">
					<button class="prev" data-carousel="properties"><i class="fa fa-chevron-left"></i></button>
					<button class="next" data-carousel="properties"><i class="fa fa-chevron-right"></i></button>
				</nav>
			</div>
		</div>
	</div>
</section>