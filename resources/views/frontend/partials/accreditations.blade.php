<section class="section accreditations">
	<div class="container">
		<div class="row">
			<header class="section-header col-md-12">
				<h3 class="section-heading">Accreditations and Partners</h3>
				<hr>
			</header>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-2"><img src="{!! asset('images/logos/arla.png') !!}" alt=""></div>
			<div class="col-sm-6 col-md-2"><img src="{!! asset('images/logos/dps.png') !!}" alt=""></div>
			<div class="col-sm-6 col-md-2"><img src="{!! asset('images/logos/naea.png') !!}" alt=""></div>
			<div class="col-sm-6 col-md-2"><img src="{!! asset('images/logos/nals.png') !!}" alt=""></div>
			<div class="col-sm-6 col-md-2"><img src="{!! asset('images/logos/property-ombudsman.png') !!}" alt=""></div>
			<div class="col-sm-6 col-md-2"><img src="{!! asset('images/logos/rightmove.png') !!}" alt=""></div>
		</div>
	</div>
</section>