<section class="section blog">
	<div class="container">
		<div class="row">
			<header class="section-header col-md-12">
				<h2 class="section-heading">Our Blog</h2>
				<hr>
			</header>
		</div>

		<div class="row posts">
			@foreach ($posts as $post)
				<div class="col-md-4">
					<article class="card">
						<a href="{!! route('blog.single', [$post->categorySlug, $post->slug]) !!}">
							@if(isset($post->thumbnail->attachment))
								<img class="card-img" src="{!! asset($post->thumbnail->attachment->url) !!}" alt="">
							@else
								<img class="card-img" src="{!! asset('images/placeholder.png') !!}" alt="Image coming soon">
							@endif
						</a>

						<div class="card-body">
							<h3 class="card-heading"><a href="{!! route('blog.single', [$post->categorySlug, $post->slug]) !!}">{!! $post->post_title !!}</a></h3>
							<a href="{!! route('blog.single', [$post->categorySlug, $post->slug]) !!}" class="card-read-more">Continue reading</a>
						</div>
					</article>
				</div>
			@endforeach
		</div>
	</div>
</section>