<section class="masthead">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@include('frontend.partials.property-search')
			</div>
		</div>
	</div>

	<ul class="slideme">
		<li style="background-image: url({!! asset('images/masthead/background.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/cover1.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/cover2.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/heywood.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/heywood-2.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/hollingworth-lake.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/milnrow.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/newhey.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/rochdale.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/surrounding-1.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/surrounding.jpg') !!})" alt=""></li>
		<li style="background-image: url({!! asset('images/masthead/toad-lane.jpg') !!})" alt=""></li>
	</ul>
</section>