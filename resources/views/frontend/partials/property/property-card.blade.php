<div class="Property">
    <a href="{!! route('properties.show', $property->property_reference) !!}">
        <figure class="Property__image-wrapper">
            @if ($property->isSold())
                <span class="tag is-sold in-corner">Sold STC</span>
            @endif
            @if($property->pictures->count())
                <img class="Property__image" src="{!! $property->pictures[0]->filename !!}" alt="{!! $property->property_reference !!}">
            @else
                <img class="Property__image" src="{!! asset('images/placeholder.png') !!}" alt="Image coming soon">
            @endif
        </figure>
        <footer class="Property__footer">
            <h3 class="Property__heading">{!! $property->street !!}</h3>
            <span class="Property__subheading">{!! $property->district !!}</span>
            <span class="Property__subheading">{!! $property->town !!}</span>

            <p class="Property__price">{!! $property->price !!}</p>
        </footer>
    </a>
</div>