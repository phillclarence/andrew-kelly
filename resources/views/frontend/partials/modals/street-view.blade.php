<div style='overflow:hidden; height:500px; width:100%;'>
    <div id='gmap_canvas' style='height:100%; width:100%;'></div>

    <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
</div>

<script type='text/javascript'>
    function init_map() {

        var location = {lat: {!! $property->latitude !!}, lng: {!! $property->longitude !!}};

        var options = {
            zoom: 1,
            position: location
        };

        var panorama = new google.maps.StreetViewPanorama(document.getElementById('gmap_canvas'), options);
    }

    init_map();
</script>