<div style='overflow:hidden; height:500px; width:100%;'>
    <div id='gmap_canvas' style='height:100%; width:100%;'></div>

    <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
</div>

<script type='text/javascript'>
    function init_map() {

        var location = {lat: {!! $property->latitude !!}, lng: {!! $property->longitude !!}};

        var options = {
            zoom: 12,
            center: new google.maps.LatLng(location),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('gmap_canvas'), options);

        marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(location)
        });
    }

    init_map();
</script>