{!! Form::open(['class' => 'ui-form']) !!}
	
	<div class="row">
		<div class="col-md-6">
			<div class="input-group">
				{!! Form::text('name', '', ['placeholder' => 'Name']) !!}
			</div>
		</div>
		<div class="col-md-6">
			<div class="input-group">
				{!! Form::email('email', '', ['placeholder' => 'Email']) !!}
			</div>
		</div>
	</div>
	
	<div class="input-group">
		{!! Form::textarea('message', '', ['placeholder' => 'Message']) !!}
	</div>

	@include('frontend.partials.forms.validation')

	<button class="ui-button">Send Message</button>

{!! Form::close() !!}