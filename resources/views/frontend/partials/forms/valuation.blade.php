<div class="container-fluid">
    <div class="row">
        <form action="{!! route('modal.store', 'valuation') !!}" method="POST" class="ui-form ui-form--white modal-form col-md-12" data-modal-type="valuation">
            <input type="hidden" name="type" value="">
            {!! csrf_field() !!}

            <div class="row">
                <header class="section-header col-md-12">
                    <h3 class="section-heading">Personal Details</h3>
                    <hr>
                </header>

                <div class="col-md-12">
                    <div class="ui-select">
                        <select name="valuation[type]" id="" required>
                            <option value="">Which type of valuation would you like?</option>
                            <option value="Sales">Sales valuation</option>
                            <option value="Rental">Rental valuation</option>
                        </select>
                    </div>
                    <input type="text" name="valuation[name]" value="" placeholder="Name" required>

                    <input type="text" name="valuation[address]" value="" placeholder="Address">

                    <input type="text" name="valuation[address1]" value="">
                </div>

                <div class="col-md-6">
                    <input type="text" name="valuation[postcode]" value="" placeholder="Postcode">

                    <input type="text" name="valuation[mobile]" value="" placeholder="Mobile">
                </div>
                <div class="col-md-6">
                    <input type="text" name="valuation[phone]" value="" placeholder="Phone">

                    <input type="text" name="valuation[email]" value="" placeholder="Email" required>
                </div>

                <div class="col-md-12">
                    <textarea name="valuation[comments]" cols="30" rows="10" placeholder="Comments"></textarea>
                </div>

                <div class="col-md-12 text-right">
                    <button class="ui-button">Send Request</button>
                </div>
            </div>
        </form>
    </div>
</div>