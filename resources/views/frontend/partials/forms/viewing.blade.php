<div class="container-fluid">
    <div class="row">
        <form action="{!! route('modal.store', 'viewing') !!}" method="POST" class="ui-form ui-form--white modal-form col-md-12" data-modal-type="viewing">
            <input type="hidden" name="type" value="">
            {!! csrf_field() !!}

            <div class="row">
                <header class="col-md-12">
                    <h3 class="heading">Personal Details</h3>
                    <hr>
                </header>

                <div class="col-md-12">
                    <div class="ui-select">
                        <select name="viewing[type]" id="" required>
                            <option value="">Which type of viewing would you like?</option>
                            <option value="Sales">Sales viewing</option>
                            <option value="Rental">Rental viewing</option>
                        </select>
                    </div>
                    <input type="text" name="viewing[name]" value="" placeholder="Name">

                    <input type="text" name="viewing[address]" value="" placeholder="Address">

                    <input type="text" name="viewing[address1]" value="" >
                </div>

                <div class="col-md-6">
                    <input type="text" name="viewing[postcode]" value="" placeholder="Postcode">

                    <input type="text" name="viewing[mobile]" value="" placeholder="Mobile">
                </div>
                <div class="col-md-6">
                    <input type="text" name="viewing[phone]" value="" placeholder="Phone">

                    <input type="text" name="viewing[email]" value="" placeholder="Email">
                </div>

                <header class="col-md-12">
                    <h3 class="heading heading--mt">Property Details</h3>
                    <hr>
                </header>

                <div class="col-md-6">
                    <input type="text" name="property[street_name]" value="" placeholder="Street Name">
                </div>

                <div class="col-md-6">
                    <input type="text" name="property[price]" value="" placeholder="Price">
                </div>

                <header class="col-md-12">
                    <h3 class="heading heading--mt">Preferred Date &amp; Time</h3>
                    <hr>
                </header>

                <div class="col-md-6">
                    <input type="text" name="viewing[date]" value="" placeholder="Date">
                </div>

                <div class="col-md-6">
                    <input type="text" name="viewing[time]" value="" placeholder="Time">
                </div>

                <div class="col-md-12 text-right">
                    <button class="ui-button">Send Request</button>
                </div>
            </div>
        </form>
    </div>
</div>