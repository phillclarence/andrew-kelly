@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('notifications'))
    <div class="alert alert-success">
        <ul>
            @foreach (session('notifications') as $notification)
                <li>{{ $notification }}</li>
            @endforeach
        </ul>
    </div>
@endif