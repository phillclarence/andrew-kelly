{!! Form::open(['route' => 'mailing', 'class' => 'ui-form']) !!}

    <header class="section-header">
        <h2 class="section-heading">Personal Details</h2>
        <hr>
    </header>

    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                {!! Form::text('name', '', ['placeholder' => 'Name']) !!}
            </div>

            <div class="input-group">
                {!! Form::text('address_1', '', ['placeholder' => 'Address']) !!}
            </div>

            <div class="input-group">
                {!! Form::text('address_2', '') !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                {!! Form::text('postcode', '', ['placeholder' => 'Postcode']) !!}
            </div>

            <div class="input-group">
                {!! Form::text('mobile', '', ['placeholder' => 'Mobile']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                {!! Form::text('phone', '', ['placeholder' => 'Home Phone']) !!}
            </div>

            <div class="input-group">
                {!! Form::email('email', '', ['placeholder' => 'Email']) !!}
            </div>
        </div>
    </div>

    <header class="section-header section-header--mt">
        <h2 class="section-heading">Property Criteria</h2>
        <hr>
    </header>

    <div class="row">
        <div class="col-md-6">
            <div class="input-group">
                {!! Form::text('min_price', '', ['placeholder' => 'Min Price']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                {!! Form::text('max_price', '', ['placeholder' => 'Max Price']) !!}
            </div>
        </div>
        <div class="col-md-12">
            <div class="input-group">
                {!! Form::textarea('areas', '', ['placeholder' => 'Preferred Areas']) !!}
            </div>
        </div>
    </div>

    @include('frontend.partials.forms.validation')

    <button class="ui-button">Submit Form</button>

{!! Form::close() !!}