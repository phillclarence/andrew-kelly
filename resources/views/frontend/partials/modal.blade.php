<div class="FormModal FormModal--{!! $modal !!}">
    <div class="FormModal__container">
        <header class="FormModal__header">
            <h5 class="FormModal__heading">{!! $heading | '' !!}</h5>
            <span class="FormModal__close"><i class="fa fa-times"></i></span>
        </header>

        <main class="FormModal__body">
            {!! $body !!}
        </main>
    </div>

    <div class="FormModal__overlay"></div>
</div>

