<section class="section testimonials">
	<div class="container">
		<div class="row">
			<header class="section-header col-md-12">
				<h2 class="section-heading">Client Comments</h2>
				<hr>
			</header>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="testimonials-carousel">
					<div class="testimonial">
						<blockquote>Having just sold my second property with Andrew Kelly I thought I would take the time to thank all concerned at the Rochdale branch and to praise the company overall. From Stephen Kellys fair valuation (Listen to his advice carefully and not your own heart when valuing a property) to Ann Burkes honest and fair recommendation of various jobs to do and tips around pricing to get the property sold.The company did almost all of the viewings which was extremely helpful in selling this property. Overall I would recommend Andrew Kelly to sell your house. My personal tip - Ring Ann !</blockquote>
						<p class="client">S Hesbrook</p>
					</div>

					<div class="testimonial">
						<blockquote>From start to finish of the sale I was dealt with in a professional manner and Carol ensured she kept me informed every step of the way.</blockquote>
						<p class="client">Kate Spencer</p>
					</div>

					<div class="testimonial">
						<blockquote>We sold our property using Andrew Kelly, the service was superb. Steve came to value the property, he was very honest and helpful. Our property sold within two weeks, we were updated and guided through the sale brilliantly by Sylvia. I would certainly recommend them.</blockquote>
						<p class="client">Gareth Williams</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>