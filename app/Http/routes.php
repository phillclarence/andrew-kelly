<?php

Route::group(['middleware' => ['web']], function () {

	# Pages Routes
    Route::get('/', ['as' => 'home', 'uses' => 'Pages\HomeController@index']);
	Route::get('/mailing-list', ['as' => 'mailing', 'uses' => 'Pages\MailingListController@index']);
	Route::post('/mailing-list', ['as' => 'mailing', 'uses' => 'Pages\MailingListController@post']);
    Route::get('/contact', ['as' => 'contact', 'uses' => 'Pages\ContactController@index']);
    Route::post('/contact', ['as' => 'contact', 'uses' => 'Pages\ContactController@post']);

    # Services Routes
    Route::get('/services', ['as' => 'services', 'uses' => 'Pages\ServicesController@index']);
    Route::get('/services/{slug}', ['as' => 'services.show', 'uses' => 'Pages\ServicesController@show']);

    # Property Routes
    Route::get('/properties', ['as' => 'properties', 'uses' => 'Property\PropertiesController@index']);
    Route::get('/properties/{reference}', ['as' => 'properties.show', 'uses' => 'Property\PropertiesController@show']);
    Route::get('/sales', ['as' => 'sales', 'uses' => 'Property\SalesController@index']);
    Route::get('/lettings', ['as' => 'lettings', 'uses' => 'Property\LettingsController@index']);
    Route::get('/auctions', ['as' => 'auctions', 'uses' => 'Property\AuctionController@index']);

	# Modal Routes
	Route::get('/modal/{slug}', ['as' => 'modal.show', 'uses' => 'Property\ModalController@show']);
	Route::get('/modal/{slug}/{property}', ['as' => 'modal.show', 'uses' => 'Property\ModalController@show']);
	Route::post('/modal/{slug}', ['as' => 'modal.store', 'uses' => 'Property\ModalController@store']);

    # Property Feed Routes
    Route::get('/feed/update', 'Property\FeedController@update');

    # Blog Routes
    Route::get('/blog', ['as' => 'blog', 'uses' => 'Blog\BlogController@index']);
    Route::get('/blog/{category}', ['as' => 'blog.category', 'uses' => 'Blog\BlogController@category']);
    Route::get('/blog/{category}/{post}', ['as' => 'blog.single', 'uses' => 'Blog\BlogController@post']);
    
});