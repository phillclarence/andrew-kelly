<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MailingListRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'address_1' => 'required',
			'postcode' => 'required',
			'phone' => '',
			'mobile' => '',
			'email' => 'required|email',
			'min_price' => 'required',
			'max_price' => 'required',
			'areas' => 'required',
        ];
    }
}
