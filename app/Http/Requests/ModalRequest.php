<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ModalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->segment(2)):
            case 'valuation':
                return $this->validateValuation();
                break;

            case 'viewing':
                return $this->validateViewing();
                break;
        endswitch;
    }

    public function validateValuation()
    {
        return [
        	'valuation.type' => 'required',
            'valuation.name' => 'required',
            'valuation.email' => 'required|email',
        ];
    }

    public function validateViewing()
    {
        return [
            'viewing.name' => 'required',
            'viewing.email' => 'required|email',
        ];
    }
}
