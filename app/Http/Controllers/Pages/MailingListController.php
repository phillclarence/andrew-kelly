<?php

namespace App\Http\Controllers\Pages;

use Mail;
use App\Http\Controllers\Controller;
use App\Http\Requests\MailingListRequest;

class MailingListController extends Controller
{
	use Contactable;

	/**
	 * ContactController constructor.
	 */
	public function __construct()
	{
		$this->to = env('EMAIL_SALES', 'hello@phillclarence.com');
	}

	/**
	 * Return mailing list view
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		return view('frontend.pages.mailing-list');
	}

	/**
	 * Handle mailing list post request and
	 * @param MailingListRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function post(MailingListRequest $request)
	{
		$input = (object) $request->input();

		Mail::send('emails.mailing-list', compact('input'), function($message) use ($input) {
			$message->from($this->from, $input->name);
			$message->to($this->to);
			$message->replyTo($input->email);
			$message->subject('Mailing List Submission');
		});

		session()->flash('notifications', ['Thanks for registering for our mailing list.']);

		return redirect()->back();
	}
}
