<?php
namespace App\Http\Controllers\Pages;

trait Contactable
{
	protected $from = 'postmaster@andrew-kelly.co.uk';
	protected $to = 'sales@andrew-kelly.co.uk';
}