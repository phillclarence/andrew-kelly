<?php

namespace App\Http\Controllers\Pages;

use Mail;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller
{
	use Contactable;

	/**
	 * ContactController constructor.
	 */
	public function __construct()
	{
		$this->to = env('EMAIL_SALES', 'sales@andrew-kelly.co.uk');
	}

	/**
     * Return contact page view
     *
     * @return mixed
     */
    public function index()
    {
    	return view('frontend.pages.contact');
    }

    /**
     * Handle contact form post
     *
     * @param ContactFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function post(ContactFormRequest $request)
    {
        $input = (object) $request->input();

        Mail::send('emails.contact', compact('input'), function($message) use ($input) {
            $message->from($this->from, $input->name);
            $message->to($this->to);
            $message->replyTo($input->email);
            $message->subject('Website Enquiry');
        });

		session()->flash('notifications', ['Message sent successfully. A member of the team will be in contact soon.']);

        return redirect()->back();
    }
}
