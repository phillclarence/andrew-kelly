<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    public function index()
    {
    	return view('frontend.services.index');
    }

    public function show($slug)
    {
    	switch($slug):
    		case 'overseas':
    			$view = 'frontend.services.overseas';
    			break;
			case 'careers':
				$view = 'frontend.services.careers';
    			break;
			case 'foundation':
				$view = 'frontend.services.foundation';
    			break;
    		case 'epc':
				$view = 'frontend.services.epc';
    			break;
			case 'conveyancing':
				$view = 'frontend.services.conveyancing';
    			break;
			case 'probate':
				$view = 'frontend.services.probate';
    			break;
			default:
				return abort('404');
    	endswitch;

    	return view($view);
    }
}
