<?php

namespace App\Http\Controllers\Blog;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Src\Blog\Repositories\BlogRepository;

class BlogController extends Controller
{
    protected $blog;

    /**
     * BlogController constructor.
     * @param $blog
     */
    public function __construct(BlogRepository $blog)
    {
        $this->blog = $blog;
    }

    public function index()
    {
        $posts = $this->blog->getAllPosts();

    	return view('frontend.blog.index', compact('posts'));
    }

    public function category($category)
    {
		$category = $this->blog->getPostsByCategorySlug($category);

        return view('frontend.blog.category', compact('category'));
    }

    public function post($category, $post)
    {
        $post = $this->blog->getPostBySlug($post);
        
        return view('frontend.blog.single', compact('post'));
    }
}
