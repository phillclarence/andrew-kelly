<?php

namespace App\Http\Controllers\Property;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LettingsController extends Controller
{
    public function index()
    {
		$type = 'lettings';
    	return view('frontend.property.lettings', compact('type'));
    }
}
