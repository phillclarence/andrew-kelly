<?php

namespace App\Http\Controllers\Property;

use App\Http\Requests;
use App\Http\Requests\ModalRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Src\Property\Repositories\PropertyRepository;
use View;

class ModalController extends Controller
{
	protected $from = 'postmaster@andrewkelly.co.uk';
	protected $to;
	protected $cc;
	protected $subject;
	protected $template;
	protected $property;

	/**
	 * PropertiesController constructor.
	 * @param $property
	 */
	public function __construct(PropertyRepository $property)
	{
		$this->property = $property;
		$this->to = env('EMAIL_SALES', 'sales@andrew-kelly.co.uk');
	}

	/**
	 * Return a modal view
	 *
	 * @param $modal
	 * @param null $propertyReference
	 * @return mixed
	 */
	public function show($modal, $propertyReference = null)
	{
		if ($propertyReference) {
			$property = $this->property->getPropertyByReference($propertyReference);
		}

		switch($modal):
			case 'viewing':
				$heading = 'Viewing Request';
				$body = View::make('frontend.partials.forms.viewing', compact('property'))->render();
				break;

			case 'valuation':
				$heading = 'Valuation Request';
				$body = View::make('frontend.partials.forms.valuation', compact('property'))->render();
				break;

			case 'map':
				$heading = 'Property Location';
				$body = View::make('frontend.partials.modals.map', compact('property'))->render();
				break;

			case 'street-view':
				$heading = 'Property Street View';
				$body = View::make('frontend.partials.modals.street-view', compact('property'))->render();
				break;

			case 'floorplan':
				if (isset($property)) {
					$property->load('floorplans');
				}
				$heading = 'Property Floor Plans';
				$body = View::make('frontend.partials.modals.floorplans', compact('property'))->render();
				break;
		endswitch;

		return view('frontend.partials.modal', compact('heading', 'body', 'modal'));
	}

	/**
	 * Handle modal submission
	 *
	 * @param ModalRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(ModalRequest $request)
	{

		switch($request->segment(2)):
			case 'viewing':
				$this->template = 'emails.viewing';
				$this->subject = 'New viewing request';
				$input = (object) $request->input('viewing');

				$this->send($input);
				break;

			case 'valuation':
				$this->template = 'emails.valuation';
				$this->subject = 'New valuation request';
				$input = (object) $request->input('valuation');

				$this->send($input);
				break;
		endswitch;

		return response()->json(['message' => 'Thanks for submitting your details, a member of the team will be in touch as soon as possible.']);
	}

	/**
	 * Send email notification
	 * 
	 * @param $input
	 */
	public function send($input)
	{
		Mail::send($this->template, compact('input'), function($message) use ($input) {
			$message->from($this->from, $input->name);
			$message->to($this->to);
			$message->replyTo($input->email);
			$message->subject($this->subject);

			if ($this->cc) {
				$message->cc($this->cc);
			}
		});
	}
}
