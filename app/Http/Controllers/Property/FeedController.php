<?php

namespace App\Http\Controllers\Property;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class FeedController extends Controller
{
    public function update()
    {
        $feed = app()->make('Src\Property\Classes\PropertyFeed');
        $feed->download()->sync()->update();
    }
}
