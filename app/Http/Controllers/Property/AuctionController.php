<?php

namespace App\Http\Controllers\Property;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuctionController extends Controller
{
    public function index()
    {
		$type = 'auctions';
    	return view('frontend.property.auction', compact('type'));
    }
}
