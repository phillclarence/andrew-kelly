<?php

namespace App\Http\Controllers\Property;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Src\Property\Repositories\PropertyRepository;

class PropertiesController extends Controller
{
    protected $property;

    /**
     * PropertiesController constructor.
     * @param $property
     */
    public function __construct(PropertyRepository $property)
    {
        $this->property = $property;
    }

	/**
	 * Return index view of all properties paginated
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function index(Request $request)
    {
		$criteria = (object) $request->input();
    	$properties = $this->property->searchProperties($criteria);

    	return view('frontend.property.index', compact('properties'));
    }

	/**
	 * Return single property view
	 * 
	 * @param $reference
	 * @return mixed
	 */
	public function show($reference)
	{
		$property = $this->property->getFullPropertyByReference($reference);

		return view('frontend.property.show', compact('property'));
	}
}
