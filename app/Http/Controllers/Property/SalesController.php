<?php

namespace App\Http\Controllers\Property;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalesController extends Controller
{
    public function index()
    {
		$type = 'sales';
    	return view('frontend.property.sales', compact('type'));
    }
}
