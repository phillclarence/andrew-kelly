<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Src\Property\Repositories\PropertyRepository',
            'Src\Property\Repositories\EloquentPropertyRepository'
        );

        $this->app->bind(
            'Src\Blog\Repositories\BlogRepository',
            'Src\Blog\Repositories\EloquentBlogRepository'
        );
    }
}
