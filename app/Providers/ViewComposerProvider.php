<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.partials.property-search', 'Src\Property\ViewComposers\PropertySearch');
        view()->composer('frontend.partials.featured-properties', 'Src\Property\ViewComposers\FeaturedProperties');
		view()->composer('frontend.partials.testimonials.lettings', 'Src\Blog\ViewComposers\Testimonials');
		view()->composer('frontend.blog.partials.sidebar', 'Src\Blog\ViewComposers\Sidebar');
		view()->composer('frontend.partials.blog', 'Src\Blog\ViewComposers\LatestPosts');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
