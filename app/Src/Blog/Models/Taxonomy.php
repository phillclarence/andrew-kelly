<?php

namespace Src\Blog\Models;

use Corcel\TermTaxonomy as Corcel;

class Taxonomy extends Corcel
{
    protected $connection = 'wordpress';
}