<?php

namespace Src\Blog\Models;

use Corcel\Post as Corcel;

class Post extends Corcel
{
    protected $connection = 'wordpress';

    public function getCategorySlugAttribute()
    {
        $terms = $this->terms;

        if(!$terms) {
            return null;
        }

        if(isset($terms['category'])) {
            return key($terms['category']);
        }

    }
}