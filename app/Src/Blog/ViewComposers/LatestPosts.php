<?php
namespace Src\Blog\ViewComposers;

use Illuminate\Contracts\View\View;
use Src\Blog\Repositories\BlogRepository;

class LatestPosts
{
	protected $blog;

	/**
	 * PropertySearch constructor.
	 * @param BlogRepository $blog
	 */
	public function __construct(BlogRepository $blog)
	{
		$this->blog = $blog;
	}

	/**
	 * Compose view with additional data for search fields
	 *
	 * @param View $view
	 */
	public function compose(View $view)
	{
		$posts = $this->blog->getAllPosts();

		$view->with('posts', $posts);
	}

}