<?php

namespace Src\Blog\Repositories;

interface BlogRepository
{
    /**
     * Return all blog posts
     *
     * @return mixed
     */
    public function getAllPosts();

    /**
     * Return posts by category slug
     *
     * @param $slug
     * @return mixed
     */
    public function getPostsByCategorySlug($slug);

    /**
     * Return single post by slug
     *
     * @param $slug
     * @return mixed
     */
    public function getPostBySlug($slug);
}