<?php

namespace Src\Blog\Repositories;

use Src\Blog\Models\Post;
use Src\Blog\Models\Category;
use Src\Blog\Models\Taxonomy;

class EloquentBlogRepository implements BlogRepository
{
    protected $post;
    protected $taxonomy;
    protected $category;

    /**
     * DatabaseBlogRepository constructor.
     * @param Post $post
     * @param Taxonomy $taxonomy
     * @param Category $category
     */
    public function __construct(
        Post $post,
        Taxonomy $taxonomy,
        Category $category
    ) {
        $this->post = $post;
        $this->taxonomy = $taxonomy;
        $this->category = $category;
    }

    /**
     * Return all blog posts
     *
     * @return mixed
     */
    public function getAllPosts()
    {
		$posts = \Cache::remember('posts', 60, function() {
			return $this->post->type('post')->status('publish')->with('taxonomies')->get();
		});

		return $posts;
    }

	/**
	 * Return latest posts by limit
	 *
	 * @param $limit
	 * @return mixed
	 */
	public function getLatestPosts($limit)
	{
		$posts = \Cache::remember('posts', 60, function() use ($limit) {
			return $this->post->type('post')->status('publish')->with('taxonomies')->limit($limit)->get();
		});

		return $posts;
	}

    /**
     * Return posts by category slug
     *
     * @param $slug
     * @return mixed
     */
    public function getPostsByCategorySlug($slug)
    {
		$categoryWithPosts = \Cache::remember('PostsByCategorySlug-'.$slug, 60, function() use ($slug) {
			return $this->category->slug($slug)->posts()->first();
		});

		return $categoryWithPosts;
    }

    /**
     * Return single post by slug
     *
     * @param $slug
     * @return mixed
     */
    public function getPostBySlug($slug)
    {
		$posts = \Cache::remember('postBySlug-'.$slug, 60, function() use ($slug) {
			return $this->post->type('post')->slug($slug)->first();
		});

		return $posts;
    }

	/**
	 * @return mixed
	 */
	public function getCategories()
	{
		$categories = \Cache::remember('categories', 60, function() {
			return $this->category->get();
		});

		return $categories;
	}

	/**
	 * Return all testimonials
	 *
	 * @return mixed
	 */
	public function getTestimonials()
	{
		$testimonials = \Cache::remember('testimonials', 60, function() {
			return $this->post->type('testimonial')->status('publish')->get();
		});

		return $testimonials;
	}
}