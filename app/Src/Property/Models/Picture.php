<?php

namespace Src\Property\Models;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    /**
     * @var string
     */
    protected $table = 'pictures';

    protected $fillable = [
        'property_id',
        'name',
        'filename',
    ];

    /**
     * Property relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo('Src\Property\Models\Property');
    }
}
