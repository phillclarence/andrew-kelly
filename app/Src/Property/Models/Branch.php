<?php

namespace Src\Property\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    /**
     * @var string
     */
    protected $table = 'branches';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Properties relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('Src\Property\Models\Property');
    }
}
