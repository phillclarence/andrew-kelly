<?php

namespace Src\Property\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyStyle extends Model
{
    /**
     * @var string
     */
    protected $table = 'property_styles';

    public $timestamps = false;

    /**
     * Property Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function property()
    {
        return $this->belongsTo('Src\Property\Models\Property');
    }
}
