<?php

namespace Src\Property\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
	const STATUS_SOLD = 'Sold STC';
	const STATUS_ON_MARKET = 'On Market';
	const STATUS_UNDER_OFFER = 'Under Offer';
	const STATUS_TO_LET = 'Available to Let';

    /**
     * @var string
     */
    protected $table = 'properties';

    protected $fillable = [
        'branch_id',
        'property_reference',
        'property_type_id',
        'property_style_id',
        'price_text',
        'numeric_price',
        'bedrooms',
        'receptions',
        'bathrooms',
        'priority',
        'tenure',
        'advert_heading',
        'main_advert',
        'advert2',
        'advert3',
        'advert4',
        'advert5',
        'advert6',
        'house_number',
        'street',
        'district',
        'town',
        'county',
        'postcode',
        'area',
        'latitude',
        'longitude',
        'newHome',
        'noChain',
        'furnished',
        'featuredProperty'
    ];

    /**
     * Pictures relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pictures()
    {
        return $this->hasMany('Src\Property\Models\Picture', 'property_id', 'id');
    }

    /**
     * Floorplans relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function floorplans()
    {
        return $this->hasMany('Src\Property\Models\Floorplan');
    }

    /**
     * Rooms relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany('Src\Property\Models\Room');
    }

	/**
	 * Is the property Sold STC?
	 *
	 * @return bool
	 */
	public function isSold()
	{
		return ($this->priority == self::STATUS_SOLD);
	}

	/**
	 * @return string
	 */
	public function getPriceAttribute()
    {
        if($this->numeric_price > 0)
            return '£' . number_format($this->numeric_price, 0);

        return 'TBC';
    }

    public function getStatusAttribute()
	{
		if ($this->priority == self::STATUS_SOLD) {
			return 'is-sold';
		}
	}
}
