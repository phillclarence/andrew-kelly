<?php

namespace Src\Property\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * @var string
     */
    protected $table = 'rooms';

    protected $fillable = [
        'property_id',
        'name',
        'measurement_text',
        'description',
    ];

    /**
     * Property relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo('Src\Property\Models\Property');
    }
}
