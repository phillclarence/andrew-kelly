<?php

namespace Src\Property\Repositories;

use DB;
use Src\Property\Models\Branch;
use Src\Property\Models\Floorplan;
use Src\Property\Models\Picture;
use Src\Property\Models\Property;
use Src\Property\Models\PropertyStyle;
use Src\Property\Models\PropertyType;
use Src\Property\Models\Room;

class EloquentPropertyRepository implements PropertyRepository
{
	const TYPE_SALES = 'sales';
	const TYPE_LETTINGS = 'lettings';
	const TYPE_AUCTIONS = 'auctions';

	const AK_SALES = [1,2,3,4,5,8];
	const AK_LETTINGS = [7];
	const AK_AUCTIONS = [6];

	const SEARCH_RADIUS = 3;
	const DISTANCE_UNIT_KILOMETERS = 111.045;
	const DISTANCE_UNIT_MILES      = 69.0;

    protected $branch;
    protected $property;
    protected $propertyStyle;
    protected $propertyType;
    protected $room;
    protected $picture;
    protected $floorplan;

    /**
     * EloquentPropertyRepository constructor.
     * @param $branch
     * @param $property
     * @param $propertyStyle
     * @param $propertyType
     * @param $room
     * @param $picture
     * @param $floorplan
     */
    public function __construct(
        Branch $branch,
        Property $property,
        PropertyStyle $propertyStyle,
        PropertyType $propertyType,
        Room $room,
        Picture $picture,
        Floorplan $floorplan
    ) {
        $this->branch = $branch;
        $this->property = $property;
        $this->propertyStyle = $propertyStyle;
        $this->propertyType = $propertyType;
        $this->room = $room;
        $this->picture = $picture;
        $this->floorplan = $floorplan;
    }

    /**
     * Store record to a given model
     *
     * @param $model
     * @param $data
     * @return mixed
     */
    public function store($model, $data)
    {
        return $this->$model->create($data);
    }

    /**
     * Get branch by name string
     *
     * @param $name
     * @return mixed
     */
    public function getBranchByName($name)
    {
        return $this->branch->where('name', 'like', '%'.$name.'%')->first();
    }

    /**
     * Get property by reference id
     *
     * @param $reference
     * @return mixed
     */
    public function getPropertyByReference($reference)
    {
        return $this->property->where('property_reference', $reference)->first();
    }

    /**
     * Update or store property record
     *
     * @param array $data
     * @param $reference
     * @return mixed
     */
    public function updateOrStoreProperty(array $data, $reference)
    {
        $property = $this->getPropertyByReference($reference);

        if(!$property) {
            $property = new $this->property;
            $property->create($data);
            return $property;
        }

        $property->update($data);
        return $property;
    }

    /**
     * Get all property attribute table records
     *
     * @param $attribute
     * @return mixed
     */
    public function getAttributes($attribute)
    {
        return $this->$attribute->all();
    }

    /**
     * Get all properties ids
     *
     * @return mixed
     */
    public function getAllPropertyIds()
    {
        return $this->property->select('id')->get();
    }

    /**
     * Return a collect of properties which exist in the property list
     *
     * @param $propertyList
     * @return mixed
     */
    public function getPropertiesToUpdate($propertyList)
    {
        return $this->property->whereIn('property_reference', $propertyList)->get();
    }

    /**
     * Return a collection of properties which no longer exist in the property list
     *
     * @param $propertyList
     * @return mixed
     */
    public function getPropertiesToDelete($propertyList)
    {
        return $this->property->whereNotIn('property_reference', $propertyList)->get();
    }

    /**
     * Remove property record by id
     *
     * @param $id
     * @return mixed
     */
    public function removeProperty($id)
    {
        return $this->property->select('id')->where('id', $id)->delete();
    }

    /**
     * Truncate property attribute tables ready for import
     *
     * @return mixed
     */
    public function truncatePropertyAttributeTables()
    {
        $this->room->truncate();
        $this->picture->truncate();
        $this->floorplan->truncate();
    }

    /**
     * Return all properties with pagination
     *
     * @param int $paginate
     * @return mixed
     */
    public function getAllProperties($paginate = 9, $orderBy = null)
    {
        $query = $this->property->query();
        $query->with('pictures');

        // Add sort ordering
        if($orderBy)
        {
            $query->orderBy($orderBy, 'ASC');
        }

        // Adds pagination
        if($paginate)
        {
            return $query->paginate($paginate);
        }

        return $query->get();
    }

    /**
     * Return all with pagination and sorting
     *
     * @param $model
     * @param null $paginate
     * @param null $orderBy
     * @return mixed
     */
    public function all($model, $paginate = null, $orderBy = null)
    {
        $query = $this->$model->query();

        // Add sort ordering
        if($orderBy)
        {
            $query->orderBy($orderBy, 'ASC');
        }

        // Adds pagination
        if($paginate)
        {
            return $query->paginate($paginate);
        }

        return $query->get();
    }

    /**
     * Get a property with it's relationships by reference id
     *
     * @param $reference
     * @return mixed
     */
    public function getFullPropertyByReference($reference)
    {
        return $this->property->with('pictures','floorplans','rooms')->where('property_reference', $reference)->first();
    }

    /**
     * Return a list for populating a dropdown
     *
     * @param $model
     * @param $column
     * @param $key
     * @return mixed
     */
    public function lists($model, $column, $key)
    {
        return $this->$model->orderBy($key, 'asc')->lists($column, $key);
    }

    /**
     * Return properties matching search criteria
     *
     * @param $criteria
     * @param int $paginate
     * @return mixed
     */
    public function searchProperties($criteria, $paginate = 12)
    {
        $query = $this->property->query();

		if (isset($criteria->lat) && isset($criteria->lng) && $criteria->lat && $criteria->lng):
			$query = $this->nearLocation($query, $criteria->lat, $criteria->lng, self::SEARCH_RADIUS, 'M');
		endif;

        if (isset($criteria->keyword) && $criteria->keyword):
			$query->where('main_advert', 'like', '%'.$criteria->keyword.'%');
			$query->where('advert_heading', 'like', '%'.$criteria->keyword.'%');
        endif;

        if (isset($criteria->propertyType) && $criteria->propertyType > 0):
            $query->where('property_type_id', $criteria->propertyType);
        endif;

        if (isset($criteria->bedrooms) && $criteria->bedrooms > 0):
            $query->where('bedrooms', $criteria->bedrooms);
        endif;

        if (isset($criteria->bathrooms) && $criteria->bathrooms > 0):
            $query->where('bathrooms', $criteria->bathrooms);
        endif;

		if (isset($criteria->type) && $criteria->type == self::TYPE_SALES) {
			$query->whereIn('branch_id', self::AK_SALES);
		}

		if (isset($criteria->type) && $criteria->type == self::TYPE_LETTINGS) {
			$query->whereIn('branch_id', self::AK_LETTINGS);

			// Change price values to use rent values instead
			$criteria->minPrice = $criteria->minRent;
			$criteria->maxPrice = $criteria->maxRent;
		}

		if (isset($criteria->type) && $criteria->type == self::TYPE_AUCTIONS) {
			$query->whereIn('branch_id', self::AK_AUCTIONS);
		}

        if (isset($criteria->minPrice) && $criteria->minPrice > 0):
            $query->where('numeric_price', '>=', $criteria->minPrice);
        endif;

        if (isset($criteria->maxPrice) && $criteria->maxPrice > 0):
            $query->where('numeric_price', '<=', $criteria->maxPrice);
        endif;

        if (isset($criteria->sort) && $criteria->sort):
            if ($criteria->sort == 'recent')
                $query->orderBy('id', 'desc');

            if ($criteria->sort == 'low-high')
                $query->orderBy('numeric_price', 'asc');

            if ($criteria->sort == 'high-low')
                $query->orderBy('numeric_price', 'desc');
        endif;


		return $query->paginate($paginate);
    }

    /**
     * Return latest properties by a given amount
     *
     * @param int $amount
     * @return mixed
     */
    public function getLatestProperties($amount = 9)
    {
        return $this->property->orderBy('id', 'DESC')->limit($amount)->get();
    }

	/**
	 * @param $query
	 * @param $lat
	 * @param $lng
	 * @param int $radius numeric
	 * @param $units string|['K', 'M']
	 * @return
	 * @throws \Exception
	 */
	public function nearLocation($query, $lat, $lng, $radius = 0.5, $units = 'K')
	{
		$distanceUnit = $this->distanceUnit($units);

		if (!(is_numeric($lat) && $lat >= -90 && $lat <= 90)) {
			throw new \Exception("Latitude must be between -90 and 90 degrees.");
		}

		if (!(is_numeric($lng) && $lng >= -180 && $lng <= 180)) {
			throw new \Exception("Longitude must be between -180 and 180 degrees.");
		}

		$haversine = sprintf('*, (%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(latitude)) * COS(RADIANS(%f - longitude)) + SIN(RADIANS(%f)) * SIN(RADIANS(latitude))))) AS distance',
			$distanceUnit,
			$lat,
			$lng,
			$lat
		);

		$subselect = clone $query;
		$subselect->selectRaw(DB::raw($haversine));

		// Optimize the query, see details here:
		// http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/

		$latDistance      = $radius / $distanceUnit;
		$latNorthBoundary = $lat - $latDistance;
		$latSouthBoundary = $lat + $latDistance;
		$subselect->whereRaw(sprintf("latitude BETWEEN %f AND %f", $latNorthBoundary, $latSouthBoundary));

		$lngDistance     = $radius / ($distanceUnit * cos(deg2rad($lat)));
		$lngEastBoundary = $lng - $lngDistance;
		$lngWestBoundary = $lng + $lngDistance;
		$subselect->whereRaw(sprintf("longitude BETWEEN %f AND %f", $lngEastBoundary, $lngWestBoundary));

		return $query->from(DB::raw('(' . $subselect->toSql() . ') as d'))->where('distance', '<=', $radius);
	}

	/**
	 * @param string $units
	 * @return float
	 * @throws \Exception
	 */
	private function distanceUnit($units = 'K')
	{
		if ($units == 'K') {
			return static::DISTANCE_UNIT_KILOMETERS;
		} elseif ($units == 'M') {
			return static::DISTANCE_UNIT_MILES;
		} else {
			throw new \Exception("Unknown distance unit measure '$units'.");
		}
	}
    
}