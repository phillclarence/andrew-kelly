<?php

namespace Src\Property\Repositories;

interface PropertyRepository
{

    /**
     * Return all with pagination and sorting
     *
     * @param $model
     * @param null $paginate
     * @param null $orderBy
     * @return mixed
     */
    public function all($model, $paginate = null, $orderBy = null);

	/**
     * Return a list for populating a dropdown
     * 
     * @param $model
     * @param $column
     * @param $key
     * @return mixed
     */
    public function lists($model, $column, $key);

    /**
     * Store record to a given model
     *
     * @param $model
     * @param $data
     * @return mixed
     */
    public function store($model, $data);

    /**
     * Get all properties ids
     *
     * @return mixed
     */
    public function getAllPropertyIds();

    /**
     * Get all property attribute table records
     *
     * @param $attribute
     * @return mixed
     */
    public function getAttributes($attribute);

    /**
     * Get branch by name string
     *
     * @param $name
     * @return mixed
     */
    public function getBranchByName($name);

    /**
     * Get property by reference id
     *
     * @param $reference
     * @return mixed
     */
    public function getPropertyByReference($reference);


	/**
     * Get a property with it's relationships by reference id
     * 
     * @param $reference
     * @return mixed
     */
    public function getFullPropertyByReference($reference);

    /**
     * Update or store property record
     *
     * @param array $data
     * @param $reference
     * @return mixed
     */
    public function updateOrStoreProperty(array $data, $reference);

    /**
     * Return a collect of properties which exist in the property list
     *
     * @param $propertyList
     * @return mixed
     */
    public function getPropertiesToUpdate($propertyList);

    /**
     * Return a collection of properties which no longer exist in the property list
     *
     * @param $propertyList
     * @return mixed
     */
    public function getPropertiesToDelete($propertyList);

	/**
     * Return latest properties by a given amount
     * 
     * @param int $amount
     * @return mixed
     */
    public function getLatestProperties($amount = 9);

    /**
     * Remove property record by id
     *
     * @param $id
     * @return mixed
     */
    public function removeProperty($id);

    /**
     * Truncate property attribute tables ready for import
     *
     * @return mixed
     */
    public function truncatePropertyAttributeTables();

    /**
     * Return all properties with pagination
     *
     * @param int $paginate
     * @return mixed
     */
    public function getAllProperties($paginate = 9);

	/**
     * Return properties matching search criteria
     * 
     * @param $criteria
     * @return mixed
     */
    public function searchProperties($criteria);

}