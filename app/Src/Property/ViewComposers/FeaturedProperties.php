<?php

namespace Src\Property\ViewComposers;

use Illuminate\Contracts\View\View;
use Src\Property\Repositories\PropertyRepository;

class FeaturedProperties {

	protected $property;

	/**
	 * PropertySearch constructor.
	 * @param $property
	 */
	public function __construct(PropertyRepository $property)
	{
		$this->property = $property;
	}


	/**
	 * Compose view with additional data for search fields
	 *
	 * @param View $view
	 */
	public function compose(View $view)
	{
		$properties = $this->property->getLatestProperties(9);

		$view->with('properties', $properties);
	}

}