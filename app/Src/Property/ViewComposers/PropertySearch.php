<?php

namespace Src\Property\ViewComposers;

use Illuminate\Contracts\View\View;
use Src\Property\Repositories\PropertyRepository;

class PropertySearch {

	protected $property;

	/**
	 * PropertySearch constructor.
	 * @param $property
	 */
	public function __construct(PropertyRepository $property)
	{
		$this->property = $property;
	}

	/**
	 * Compose view with additional data for search fields
	 *
	 * @param View $view
	 */
	public function compose(View $view)
	{
		// Property Types
		$propertyTypes = $this->property->lists('propertyType', 'name', 'id');
		$propertyTypes->prepend('Property Type (any)');
		
		// Bedrooms
		$bedrooms = array('Bedrooms (any)','1','2','3','4','5');

		// Bathrooms
		$bathrooms = array('Bathrooms (any)','1','2','3','4');

		$prices = $this->getPriceRange();
		$rent = $this->getRentRange();

		$view->with('propertyTypes', $propertyTypes);
		$view->with('bedrooms', $bedrooms);
		$view->with('bathrooms', $bathrooms);
		$view->with('minPrice', $prices['min']);
		$view->with('maxPrice', $prices['max']);
		$view->with('minRent', $rent['min']);
		$view->with('maxRent', $rent['max']);
	}

	/**
	 * Get min and max price range lists
	 *
	 * @return mixed
	 */
	private function getPriceRange()
	{
		$min = $this->generateRange(50000, 1000000, 10000);
		$max = $min;

		$placeholder = ['' => 'Price From'];
		$min = array_replace($placeholder, $min);

		$placeholder = ['' => 'Price To'];
		$max = array_replace($placeholder, $max);

		// Prepend placeholder text
		$data['min'] = $min;
		$data['max'] = $max;

		return $data;
	}

	private function getRentRange()
	{
		$min = $this->generateRange(100, 1500, 100);
		$min['1500'] = '£1,500+';
		$max = $min;

		$placeholder = ['' => 'Rent From'];
		$min = array_replace($placeholder, $min);

		$placeholder = ['' => 'Rent To'];
		$max = array_replace($placeholder, $max);

		// Prepend placeholder text
		$data['min'] = $min;
		$data['max'] = $max;

		return $data;
	}

	/**
	 * Returns price range array
	 *
	 * @param $min
	 * @param $max
	 * @param $increment
	 * @return mixed
	 */
	private function generateRange($min, $max, $increment)
	{
		foreach (range($min, $max, $increment) as $price) {
			$prices[$price] = '£' . number_format($price, 0, ".", ",");
		}
		return $prices;
	}
}