<?php

namespace Src\Property\Classes;

use Log;
use Storage;
use XmlParser;
use Carbon\Carbon;
use Src\Property\Repositories\PropertyRepository;

class PropertyFeed
{
    protected $feed;
    protected $feedPath;
    protected $property;
    protected $xml;
    protected $types;
    protected $styles;
    protected $propertyList;

    private $executionStart;
    private $executionTime;

    /**
     * PropertyFeed constructor.
     * @param $property
     */
    public function __construct(PropertyRepository $property)
    {
        // Start execution timer
        $this->executionStart = $this->_microtime_float();

        $this->property = $property;

        // Store local collections to save on mass queries
        $this->types = $this->property->getAttributes('propertyType');
        $this->styles = $this->property->getAttributes('propertyStyle');

        // Truncate property attribute tables
        $this->property->truncatePropertyAttributeTables();
    }

    /**
     * Download latest feed from expert agent ftp
     *
     */
    public function download()
    {
        $timestamp = Carbon::now()->format('ga-d-m-Y'); // 8am-01-01-2016
        $this->feed = $timestamp . '-properties.xml';

        $this->feedPath = Storage::disk('feeds')->getDriver()->getAdapter()->getPathPrefix() . $this->feed;

        if(Storage::disk('feeds')->exists($this->feed))
        {
            // Log an attempt where the file already exists
            Log::info('File '.$this->feed.' already downloaded - did the feed cron run too soon?');
        } else {
            // Download and store feed to local storage
            $contents = Storage::disk('expertagent')->get('properties.xml');
            Storage::disk('feeds')->put($this->feed, $contents);
        }

        // Set feed xml
        $this->_loadFeedXml();

        return $this;
    }

    /**
     * Synchronise properties - delete or update
     *
     */
    public function sync()
    {
        $this->propertyList = $this->_buildPropertyList();

        $deleteList = $this->property->getPropertiesToDelete($this->propertyList);

        if($deleteList):
            $this->_processDeleteList($deleteList);
        endif;

        return $this;
    }

    /**
     * Update property records
     *
     */
    public function update()
    {
        foreach($this->xml->branches->branch as $xml):
            $this->_processBranch($xml);
        endforeach;

        $end = $this->_microtime_float();
        $this->executionTime = $end - $this->executionStart . 'ms';

        dd($this);
    }

    /**
     * Load the xml feed
     *
     */
    private function _loadFeedXml()
    {
        $feed = Storage::disk('feeds')->get($this->feed);
        $this->xml = new \SimpleXMLElement($feed);
    }

    /**
     * Process each branch in the xml file
     *
     * @param $xml
     */
    private function _processBranch($xml)
    {
        // Extract branch name from attributes
        $attributes = $xml[0]->attributes();
        $name = (string) $attributes['name'];

        // Get branch id based on name
        $branch = $this->property->getBranchByName($name);

        // Loop through branch properties
        foreach($xml->properties as $properties):
            $this->_processProperty($properties, $branch);
        endforeach;
    }

    /**
     * Process an individual property
     *
     * @param $properties
     * @param $branch
     */
    private function _processProperty($properties, $branch)
    {
        foreach($properties as $xml):

            $reference = (string) $xml->property_reference;

            $record = $this->_buildProperty($xml, $branch);

            // Store or update property record
            $property = $this->property->updateOrStoreProperty($record, $reference);

            // Store property rooms
            $this->_processRooms($property->id, $xml);

            // Store property pictures
            $this->_processPictures($property->id, $xml);

            // Store property floorplans
            $this->_processFloorplans($property->id, $xml);

        endforeach;
    }

    /**
     * Process delete list to remove properties
     *
     * @param $deleteList
     */
    private function _processDeleteList($deleteList)
    {
        foreach($deleteList as $property):
            $this->property->removeProperty($property->id);
        endforeach;
    }

    /**
     * Build array of property references from xml file
     *
     * @return array
     */
    private function _buildPropertyList()
    {
        $list = [];

        foreach($this->xml->branches->branch as $branch):
            foreach($branch->properties as $properties):
                foreach($properties as $xml):
                    $list[] .= $xml->property_reference;
                endforeach;
            endforeach;
        endforeach;

        return $list;
    }

    /**
     * Build array for property store / update
     *
     * @param $xml
     * @param $branch
     * @return array
     */
    private function _buildProperty($xml, $branch)
    {
        return [
            'branch_id' => $branch->id,
            'property_reference' => (int) $xml->property_reference,
            'property_type_id' => $this->_getAttributeIdByName('types', $xml->property_type),
            'property_style_id' => $this->_getAttributeIdByName('styles', $xml->property_style),
            'price_text' => (string) $xml->price_text,
            'numeric_price' => (double) $xml->numeric_price,
            'bedrooms' => (int) $xml->bedrooms,
            'receptions' => (int) $xml->receptions,
            'bathrooms' => (int) $xml->bathrooms,
            'priority' => (string) $xml->priority,
            'tenure' => (string) $xml->tenure,
            'advert_heading' => (string) $xml->advert_heading,
            'main_advert' => (string) $xml->main_advert,
            'advert2' => (string) $xml->advert2,
            'advert3' => (string) $xml->advert3,
            'advert4' => (string) $xml->advert4,
            'advert5' => (string) $xml->advert5,
            'advert6' => (string) $xml->advert6,
            'house_number' => (string) $xml->house_number,
            'street' => (string) $xml->street,
            'district' => (string) $xml->district,
            'town' => (string) $xml->town,
            'county' => (string) $xml->county,
            'postcode' => (string) $xml->postcode,
            'area' => (string) $xml->area,
            'latitude' => (float) $xml->latitude,
            'longitude' => (float) $xml->longitude,
            'new_home' => $this->_convertToBool($xml->newHome),
            'no_chain' => $this->_convertToBool($xml->noChain),
            'furnished' => (string) $xml->furnished,
            'featured_property' => $this->_convertToBool($xml->featuredProperty),
        ];
    }

    /**
     * Process property rooms
     *
     * @param $propertyID
     * @param $xml
     */
    private function _processRooms($propertyID, $xml)
    {
        foreach($xml->rooms as $room):

            foreach($room as $data):

                // Extract attributes
                $attributes = $data->attributes();

                $record = [
                    'property_id' => $propertyID,
                    'name' => (string) $attributes['name'],
                    'measurement_text' => (string) $data->measurement_text,
                    'description' => (string) $data->description,
                ];

                $this->property->store('room', $record);
            endforeach;
        endforeach;
    }

    /**
     * Process property pictures
     *
     * @param $propertyID
     * @param $xml
     */
    private function _processPictures($propertyID, $xml)
    {
        foreach($xml->pictures as $picture):
            foreach($picture as $data):

                // Extract attributes
                $attributes = $data->attributes();

                $record = [
                    'property_id' => $propertyID,
                    'name' => (string) $attributes['name'],
                    'filename' => (string) $data->filename,
                ];

                $this->property->store('picture', $record);

            endforeach;
        endforeach;
    }

    /**
     * Process property floorplans
     *
     * @param $propertyID
     * @param $xml
     */
    private function _processFloorplans($propertyID, $xml)
    {
        foreach($xml->floorplans as $floorplan):
            foreach($floorplan as $data):

                // Extract attributes
                $attributes = $data->attributes();

                $record = [
                    'property_id' => $propertyID,
                    'name' => (string) $attributes['name'],
                    'filename' => (string) $data->filename,
                ];

                $this->property->store('floorplan', $record);

            endforeach;
        endforeach;
    }

    /**
     * Convert a string Yes/No value to boolean
     *
     * @param $value
     * @return bool
     */
    private function _convertToBool($value)
    {
        $value = (string) $value;

        if($value == 'NO')
            return false;

        if($value == 'YES')
            return true;
    }

    /**
     * Get attribute id from stored collection using name value
     *
     * @param $attribute
     * @param $name
     * @return mixed
     */
    private function _getAttributeIdByName($attribute, $name)
    {
        $name = (string) $name;

        $item = $this->$attribute->where('name', $name)->first();

        if(!$item)
            return 0;

        return $item->id;
    }

    /**
     * Return microtime timestamp
     *
     * @return float
     */
    private function _microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

}